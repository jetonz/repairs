﻿using System;
using System.Windows;

namespace WorkShop.Views.Repairs
{
    /// <summary>
    /// Interaction logic for AddRepair.xaml
    /// </summary>
    public partial class RepairDetailsWindow : Window
    {

        public RepairDetailsWindow(RepairDetailsViewModel viewModel)
        {

            InitializeComponent();
            this.DataContext = viewModel;

            if (String.IsNullOrEmpty(viewModel.IMEI))
            {
                this.Title = "Добавить ремонт";
            }
            else
            {
                this.Title = String.Format("Ремонт №{0}", viewModel.Id);
            }

            this.AddClient.Click += (s, e) =>
            {
                DialogWindow win = new DialogWindow();
                win.Title = "Добавить владельца";
                win.OkButton.Click += (n, g) =>
                {
                    viewModel.CreateClient(win.Val.Text);
                };
                win.ShowDialog();
            };

            this.PriceAgrred.Click += (s, e) =>
            {
                viewModel.SetPriceAgreed();
            };

            #region Handlers
            this.CloseCancle.Click += (s, e) =>
            {
                this.Close();
            };

            this.CloseOk.Click += (s, e) =>
            {
                viewModel.SaveRepair();
                if (String.IsNullOrEmpty(viewModel.Error))
                {
                    this.Close();
                }
            };

            this.Print.Click += (s, e) =>
            {
                viewModel.SaveRepair();
                if (String.IsNullOrEmpty(viewModel.Error))
                {
                    viewModel.PrintRepair();
                }
            };

            this.Price.PreviewTextInput += (s, e) =>
             {
                 e.Handled = !e.Text.IsNumber();
             };

            this.WorkPrice.PreviewTextInput += (s, e) =>
            {
                e.Handled = !e.Text.IsNumber();
            }; 

            this.PartPrice.PreviewTextInput += (s, e) =>
            {
                e.Handled = !e.Text.IsNumber();
            };

            this.KeyDown += (s, e) =>
            {
                if(e.Key == System.Windows.Input.Key.Escape)
                {
                    this.Close();
                }
            };

            #endregion


        }
 
    }
}
