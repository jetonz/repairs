﻿using System;

namespace WorkShop.Models.Entities
{
    public class Breakage : BaseEntity<Guid>
    {
        public decimal InternalPrice { get; set; }

        public decimal ExternalPrice { get; set; }

        public string Description { get; set; }

        public bool IsDeleted { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}