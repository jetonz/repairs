﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WorkShop.Models.Entities
{
    public class User : BaseEntity<Guid> 
    {
        public User()
        {
            this.AcceptedRepairs = new HashSet<Repair>();
            this.RepairEditHistory = new HashSet<RepairEditHistory>();
        }

        public string Name { get; set; } 
        public string Password { get; set; }
        public string Login { get; set; }

        public virtual ICollection<Repair> AcceptedRepairs { get; set; }

        public virtual Role Role { get; set; }

        public virtual ICollection<RepairEditHistory> RepairEditHistory { get; set; }
 
        public override string ToString()
        {
            return this.Name;
        }

        
    }
}
