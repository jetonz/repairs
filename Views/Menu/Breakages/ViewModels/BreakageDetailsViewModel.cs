﻿using System;
using System.ComponentModel;
using WorkShop.Database;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;

namespace WorkShop.Views.Menu.Breakages
{
    public class BreakageDetailsViewModel : BaseViewModel, IDataErrorInfo
    {
        public Action OnModelSaved { get; set; }

        public BreakageDetailsViewModel()
        {

        }

        public BreakageDetailsViewModel(Guid id) : this()
        {
            Id = id;
        }

        public Guid Id { get; private set; }

        private string _description;
        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
                OnPropertyChanged(nameof(IsModelValid));
            }
        }

        private decimal _externalPrice;
        public decimal ExternalPrice
        {
            get => _externalPrice;
            set
            {
                _externalPrice = value;
                OnPropertyChanged(nameof(ExternalPrice));
                OnPropertyChanged(nameof(IsModelValid));
            }
        }

        private decimal _internalPrice;
        public decimal InternalPrice
        {
            get => _internalPrice;
            set
            {
                _internalPrice = value;
                OnPropertyChanged(nameof(InternalPrice));
                OnPropertyChanged(nameof(IsModelValid));
            }
        }

        public bool IsModelValid
        {
            get
            {
                return !string.IsNullOrEmpty(Description)
                    && !string.IsNullOrWhiteSpace(Description)
                    && ExternalPrice > 0
                    && InternalPrice > 0;
            }
        }

        public string Error => throw new NotImplementedException();

        public string this[string columnName]
        {
            get
            {
                string error = string.Empty;
                switch (columnName)
                {
                    case nameof(Description):
                        if (string.IsNullOrEmpty(Description) || string.IsNullOrWhiteSpace(Description))
                        {
                            error = "Укажите название поломки";
                        }
                        break;
                    case nameof(InternalPrice):
                        if (InternalPrice <= 0)
                        {
                            error = "Укажите цену больше нуля";
                        }
                        break;
                    case nameof(ExternalPrice):
                        if (ExternalPrice <= 0)
                        {
                            error = "Укажите цену больше нуля";
                        }
                        break;
                }

                return error;
            }
        }

        public void SaveChanges()
        {
            if (!IsModelValid)
            {
                return;
            }

            using (var context = ContextFactory.CreateContext())
            {
                if (Id == Guid.Empty)
                {
                    var dbBreakage = new Breakage
                    {
                        Description = Description,
                        InternalPrice = InternalPrice,
                        ExternalPrice = ExternalPrice,
                        Id = Guid.NewGuid()
                    };
                    context.Breakages.Add(dbBreakage);
                }
                else
                {
                    var dbBreakage = context.Breakages.Find(Id);
                    dbBreakage.InternalPrice = InternalPrice;
                    dbBreakage.ExternalPrice = ExternalPrice;
                    dbBreakage.Description = Description;
                }

                context.SaveChanges();
            }

            OnModelSaved?.Invoke();
        }


    }
}
