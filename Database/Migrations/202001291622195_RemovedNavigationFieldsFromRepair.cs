﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedNavigationFieldsFromRepair : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Repair", "ClientId", "dbo.Clients");
            DropIndex("dbo.Repair", new[] { "ClientId" });
            RenameColumn(table: "dbo.Repair", name: "ClientId", newName: "Client_Id");
            RenameColumn(table: "dbo.Repair", name: "AccepterId", newName: "Accepter_Id");
            RenameColumn(table: "dbo.Repair", name: "MasterId", newName: "Master_Id");
            RenameColumn(table: "dbo.Repair", name: "PriceAgreedById", newName: "PriceAgreedBy_Id");
            RenameIndex(table: "dbo.Repair", name: "IX_MasterId", newName: "IX_Master_Id");
            RenameIndex(table: "dbo.Repair", name: "IX_AccepterId", newName: "IX_Accepter_Id");
            RenameIndex(table: "dbo.Repair", name: "IX_PriceAgreedById", newName: "IX_PriceAgreedBy_Id");
            AlterColumn("dbo.Repair", "Client_Id", c => c.Guid());
            CreateIndex("dbo.Repair", "Client_Id");
            AddForeignKey("dbo.Repair", "Client_Id", "dbo.Clients", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Repair", "Client_Id", "dbo.Clients");
            DropIndex("dbo.Repair", new[] { "Client_Id" });
            AlterColumn("dbo.Repair", "Client_Id", c => c.Guid(nullable: false));
            RenameIndex(table: "dbo.Repair", name: "IX_PriceAgreedBy_Id", newName: "IX_PriceAgreedById");
            RenameIndex(table: "dbo.Repair", name: "IX_Accepter_Id", newName: "IX_AccepterId");
            RenameIndex(table: "dbo.Repair", name: "IX_Master_Id", newName: "IX_MasterId");
            RenameColumn(table: "dbo.Repair", name: "PriceAgreedBy_Id", newName: "PriceAgreedById");
            RenameColumn(table: "dbo.Repair", name: "Master_Id", newName: "MasterId");
            RenameColumn(table: "dbo.Repair", name: "Accepter_Id", newName: "AccepterId");
            RenameColumn(table: "dbo.Repair", name: "Client_Id", newName: "ClientId");
            CreateIndex("dbo.Repair", "ClientId");
            AddForeignKey("dbo.Repair", "ClientId", "dbo.Clients", "Id", cascadeDelete: true);
        }
    }
}
