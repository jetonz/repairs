﻿using WorkShop.Database;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WorkShop.Views.Menu.Roles
{
    public class RolesListViewModel : BaseViewModel
    {
        private ObservableCollection<RoleGridItem> _rolesList;
        public IReadOnlyCollection<RoleGridItem> RolesList
        {
            get
            {
                return _rolesList;
            }
        }

        private RoleGridItem _gridItem;
        public RoleGridItem SelectedItem
        {
            get => _gridItem;
            set
            {
                _gridItem = value;
                OnPropertyChanged(nameof(SelectedItemBeDeleted));
                OnPropertyChanged(nameof(SelectedItem));
            }
        }

        public bool SelectedItemBeDeleted => SelectedItem?.Role.Users?.Count == 0;


        public RolesListViewModel()
        {
            LoadRoles();
        }

        public void DeleteSelectedRole()
        {
            using (var context = ContextFactory.CreateContext())
            {
                var roleToDelete = context.Roles.Find(SelectedItem.Role.Id);
                context.Entry(roleToDelete).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
                SelectedItem = null;
                OnPropertyChanged(nameof(RolesList));
            }
        }

        public void LoadRoles()
        {
            using (var context = ContextFactory.CreateContext())
            {
                _rolesList = new ObservableCollection<RoleGridItem>(
                    context.Roles
                    .Include(nameof(Role.Users))
                    .ToArray()
                    .Select(x => new RoleGridItem(x))
                   );

                SelectedItem = null;
                OnPropertyChanged(nameof(RolesList)); 
            }
        }
    }
}
