﻿namespace WorkShop.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class IsDeletedBreakage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Breakages", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Breakages", "IsDeleted");
        }
    }
}
