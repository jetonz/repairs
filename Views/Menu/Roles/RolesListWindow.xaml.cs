﻿using WorkShop.Models.Entities;
using System.Windows;
using WorkShop.Models;
using WorkShop.Database;
using System.Linq;
using System.Data.Entity;

namespace WorkShop.Views.Menu.Roles
{
    /// <summary>
    /// Interaction logic for RolesListView.xaml
    /// </summary>
    public partial class RolesListWindow : Window
    {
        public RolesListWindow()
        {
            InitializeComponent();
            RolesListViewModel viewModel = new RolesListViewModel();
            this.DataContext = viewModel;

            this.BtnEdit.Click += (s, e) =>
            {
                RoleDetailsWindow win = new RoleDetailsWindow(viewModel.SelectedItem.Role.Id);
                win.Title = "Редактировать роль";
                win.Closing += (s1, e1) =>
                {
                    viewModel.LoadRoles();
                    // TODO: REMOVE
                    var currUserId = ApplicationUser.GetInstance().DbUser.Id;
                    var updatedUser = ContextFactory.CreateContext().Users
                        .Where(x => x.Id == currUserId)
                        .Include(x => x.Role)
                        .First();

                    ApplicationUser.SetUser(updatedUser);

                };
                
                win.ShowDialog();

            };

            this.BtnCreate.Click += (s, e) =>
            {
                RoleDetailsWindow win = new RoleDetailsWindow();
                win.Title = "Создать роль";
                win.Closing += (s1, e1) =>
                {
                    viewModel.LoadRoles();
                };

                win.ShowDialog();

            };

            this.BtnDelete.Click += (s, e) =>
            {
                viewModel.DeleteSelectedRole();
                viewModel.LoadRoles();
            };
 
        }
    }
}
