﻿using WorkShop.Database;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace WorkShop.Views.Menu.Masters
{
    public class MastersListViewModel : BaseViewModel
    {
        private ObservableCollection<Master> _MastersList;
        public ObservableCollection<Master> MastersList
        {
            get
            {
                return _MastersList;
            }
            set
            {
                _MastersList = value;
                OnPropertyChanged("MastersList");
            }
        }

        RepairsContext DbConnection;
        public Master CurrMaster { get; set; }

        //public ICommand Edit { get; private set; }
        //public ICommand Create { get; private set; }
        //public ICommand Delete { get; private set; }

        public MastersListViewModel()
        {
            DbConnection = ContextFactory.CreateContext();
            MastersList = new ObservableCollection<Master>(DbConnection.Masters.ToList());

            //Edit = new RelayCommand(EditStatus);
            //Create = new RelayCommand(CreateStatus);
            //Delete = new RelayCommand(DeleteStatus);

        }

        public void EditMaster()
        {
            DbConnection.Entry(CurrMaster).State = System.Data.Entity.EntityState.Modified;
            DbConnection.SaveChanges();
        }

        public void CreateMaster(string MasterName)
        {
            Master master = new Master() { Id = Guid.NewGuid(), Name = MasterName };
            DbConnection.Masters.Add(master);
            DbConnection.SaveChanges();
            MastersList.Add(master);
        }

        public void DeleteMaster(Master master)
        {
            DbConnection.Masters.Remove(master);
            if (DbConnection.SaveChanges() == 1)
            {
                MastersList.Remove(master);
                CurrMaster = null;
            }
        }
    }
}
