﻿namespace WorkShop.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PriceAgreed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Repair", "PriceAgreedById", c => c.Guid());
            AddColumn("dbo.Repair", "PriceAgreedAt", c => c.DateTime());
            CreateIndex("dbo.Repair", "PriceAgreedById");
            AddForeignKey("dbo.Repair", "PriceAgreedById", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Repair", "PriceAgreedById", "dbo.Users");
            DropIndex("dbo.Repair", new[] { "PriceAgreedById" });
            DropColumn("dbo.Repair", "PriceAgreedAt");
            DropColumn("dbo.Repair", "PriceAgreedById");
        }
    }
}
