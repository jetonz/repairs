﻿using System;

namespace WorkShop.Contracts
{
    public interface IComboboxedItem
    {
        Guid Id { get; set; }
        string Name { get; set; }
    }
}
