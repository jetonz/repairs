﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace WorkShop.Converters
{
    [ValueConversion(typeof(string),typeof(decimal))]
    class EmptyStringToZeroConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(Decimal.TryParse(value?.ToString(), out decimal _res))
            {
                if(_res == 0)
                {
                    return "";
                } else
                {
                   return _res.ToString();
                }
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value as string))
            {
                return 0;
            }
            else
            { 
                return Decimal.TryParse(value as string, out decimal _res)
                    ? _res
                    : 0;
            }
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
