﻿using WorkShop.Extentions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace WorkShop.Converters
{

    [ValueConversion(typeof(Enum), typeof(IEnumerable<KeyValuePair<object, object>>))]
    public class EnumToCollectionConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumsWithDescr = EnumHelper.GetAllValuesAndDescriptions(value.GetType());
            return enumsWithDescr;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
