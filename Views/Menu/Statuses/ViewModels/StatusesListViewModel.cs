﻿using WorkShop.Database;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace WorkShop.Views.Menu.Statuses
{
    public class StatusesListViewModel : BaseViewModel
    {
        private ObservableCollection<Status> _StatusesList;
        public ObservableCollection<Status> StatusesList
        {
            get
            {
                return _StatusesList;
            }
            set
            {
                _StatusesList = value;
                OnPropertyChanged("StatusesList");
            }
        }


        RepairsContext DbConnection;
        public Status CurrStatus { get; set; }

        //public ICommand Edit { get; private set; }
        //public ICommand Create { get; private set; }
        //public ICommand Delete { get; private set; }

        public StatusesListViewModel()
        {
            DbConnection = ContextFactory.CreateContext();
            StatusesList = new ObservableCollection<Status>(DbConnection.Statuses.ToList());

            //Edit = new RelayCommand(EditStatus);
            //Create = new RelayCommand(CreateStatus);
            //Delete = new RelayCommand(DeleteStatus);

        }

        public void EditStatus()
        {
            DbConnection.Entry(CurrStatus).State = System.Data.Entity.EntityState.Modified;
            StatusesList = new ObservableCollection<Status>(DbConnection.Statuses.ToList());
            DbConnection.SaveChanges();
        }

        public void CreateStatus(string statusName, bool IsIssued)
        {
            Status status = new Status
            {
                Id = Guid.NewGuid(),
                Name = statusName,
                IsIssueStatus = IsIssued
            };
            DbConnection.Statuses.Add(status);
            StatusesList.Add(status);
            DbConnection.SaveChanges();
        }

        public void DeleteStatus(Status status)
        {
            DbConnection.Statuses.Remove(status);
            if (DbConnection.SaveChanges() == 1)
            {
                StatusesList.Remove(status);
                CurrStatus = null;
            }
        }
    }
}
