﻿ 
using System.Windows;
using System;
using System.Windows.Forms;
using System.Windows.Media;
using System.IO;

namespace WorkShop.Views.Menu.Roles
{
    /// <summary>
    /// Interaction logic for StatusView.xaml
    /// </summary>
    public partial class RoleDetailsWindow : Window
    {
        private readonly RoleDetailsViewModel _viewModel;

        public RoleDetailsWindow()
        {
            InitializeComponent();
            _viewModel = new RoleDetailsViewModel();
            this.DataContext = _viewModel;
            BindButtons();
         
        }

        public RoleDetailsWindow(Guid roleId)
        {
            InitializeComponent();
            _viewModel = new RoleDetailsViewModel(roleId);
            this.DataContext = _viewModel;
            BindButtons();
          

        }

        private void BindButtons()
        {
            this.OkButton.Click+= (s,e) =>
            {
                _viewModel.Save();
                this.Close();
            };

            this.CancleButton.Click += (s, e) =>
            {
                this.Close();
            };

            this.HelpButton.Click += (s, e) =>
            {
                System.Windows.Controls.Image imageControl = new System.Windows.Controls.Image
                {
                    Source = (new ImageSourceConverter()).ConvertFromString("pack://application:,,,/Resources/roles_help.png") as ImageSource
                };

                Window form = new Window
                {
                    Content = imageControl,
                    Width = 845,
                    Height = 775
                };

                form.Show();
            };
             
        }

    }
}
