﻿namespace WorkShop.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class BreakageAndRolesTables : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Owners", newName: "Clients");
            RenameColumn(table: "dbo.Repair", name: "OwnerId", newName: "ClientId");
            RenameIndex(table: "dbo.Repair", name: "IX_OwnerId", newName: "IX_ClientId");
            CreateTable(
                "dbo.Breakages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        InternalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExternalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Repair", "Breakage_Id", c => c.Guid());
            AddColumn("dbo.Roles", "CanEditBreakage", c => c.Boolean(nullable: false));
            AddColumn("dbo.Clients", "IsExternal", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Repair", "Breakage_Id");
            AddForeignKey("dbo.Repair", "Breakage_Id", "dbo.Breakages", "Id");

            RenameColumn("dbo.Roles", "CanCreateOwner", "CanCreateClients"); //Manual
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Roles", "CanCreateClients","CanCreateOwner"); //Manual

            DropForeignKey("dbo.Repair", "Breakage_Id", "dbo.Breakages");
            DropIndex("dbo.Repair", new[] { "Breakage_Id" });
            DropColumn("dbo.Clients", "IsExternal");
            DropColumn("dbo.Roles", "CanEditBreakage");
            DropColumn("dbo.Repair", "Breakage_Id");
            DropTable("dbo.Breakages");
            RenameIndex(table: "dbo.Repair", name: "IX_ClientId", newName: "IX_OwnerId");
            RenameColumn(table: "dbo.Repair", name: "ClientId", newName: "OwnerId");
            RenameTable(name: "dbo.Clients", newName: "Owners");
        }
    }
}
