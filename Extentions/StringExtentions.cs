﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace System
{
    public static class StringExtentions
    {
        public static bool IsNumber(this string text)
        { 
            Regex regex = new Regex("[^0-9.-]+"); 
            return !regex.IsMatch(text);
        }
    }
}
