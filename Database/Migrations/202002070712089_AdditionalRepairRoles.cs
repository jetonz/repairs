﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdditionalRepairRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeClient", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanAddClients", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeMasters", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeePriority", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeAccepter", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeePrice", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeReason", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeHistory", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeHistory");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeReason");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeePrice");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeAccepter");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeePriority");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeStatus");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeMasters");
            DropColumn("dbo.Roles", "RepairPermissions_CanAddClients");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeClient");
        }
    }
}
