﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class FieldsUpdate : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Repair", "RepairPrice", "Price");
            DropColumn("dbo.Repair", "SelfPrice");
            DropColumn("dbo.Statuses", "UseFormula");
        }

        public override void Down()
        {
            AddColumn("dbo.Statuses", "UseFormula", c => c.Boolean(nullable: false));
            AddColumn("dbo.Repair", "SelfPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            RenameColumn("dbo.Repair", "Price", "RepairPrice");
        }
    }
}
