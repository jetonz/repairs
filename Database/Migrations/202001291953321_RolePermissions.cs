﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RolePermissions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Roles", "ApplicationPermissions_HasUsersAccess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "ApplicationPermissions_HasMastersAccess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "ApplicationPermissions_HasReportsAccess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "ApplicationPermissions_HasStatusesAccess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "ApplicationPermissions_HasRolesAccess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "ApplicationPermissions_HasClientsAccess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "ApplicationPermissions_HasBreakagesAccess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanViewAllRepairs", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanCreateRepair", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanEditAnyRepair", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeSelfPrices", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "RepairPermissions_CanSeePhone", c => c.Boolean(nullable: false));
            DropColumn("dbo.Roles", "CanCreateUser");
            DropColumn("dbo.Roles", "CanCreateMaster");
            DropColumn("dbo.Roles", "CanCreateReport");
            DropColumn("dbo.Roles", "CanCreateStatus");
            DropColumn("dbo.Roles", "CanCreateRoles");
            DropColumn("dbo.Roles", "CanCreateClients");
            DropColumn("dbo.Roles", "CanEditPrice");
            DropColumn("dbo.Roles", "CanCreateRepair");
            DropColumn("dbo.Roles", "CanSeeSelfPrice");
            DropColumn("dbo.Roles", "CanEditBreakage");
            DropColumn("dbo.Roles", "CanSeePhone");
            DropColumn("dbo.Roles", "CanEditMaster");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Roles", "CanEditMaster", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanSeePhone", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanEditBreakage", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanSeeSelfPrice", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanCreateRepair", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanEditPrice", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanCreateClients", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanCreateRoles", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanCreateStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanCreateReport", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanCreateMaster", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "CanCreateUser", c => c.Boolean(nullable: false));
            DropColumn("dbo.Roles", "RepairPermissions_CanSeePhone");
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeSelfPrices");
            DropColumn("dbo.Roles", "RepairPermissions_CanEditAnyRepair");
            DropColumn("dbo.Roles", "RepairPermissions_CanCreateRepair");
            DropColumn("dbo.Roles", "RepairPermissions_CanViewAllRepairs");
            DropColumn("dbo.Roles", "ApplicationPermissions_HasBreakagesAccess");
            DropColumn("dbo.Roles", "ApplicationPermissions_HasClientsAccess");
            DropColumn("dbo.Roles", "ApplicationPermissions_HasRolesAccess");
            DropColumn("dbo.Roles", "ApplicationPermissions_HasStatusesAccess");
            DropColumn("dbo.Roles", "ApplicationPermissions_HasReportsAccess");
            DropColumn("dbo.Roles", "ApplicationPermissions_HasMastersAccess");
            DropColumn("dbo.Roles", "ApplicationPermissions_HasUsersAccess");
        }
    }
}
