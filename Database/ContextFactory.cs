﻿using System.Data.Entity.Infrastructure;

namespace WorkShop.Database
{
    public class ContextFactory : IDbContextFactory<RepairsContext>
    {
        public ContextFactory()
        {
        }

        public RepairsContext Create()
        {
            return ContextFactory.CreateContext();
        }


        public static RepairsContext CreateContext()
        {
            return new RepairsContext();
        }
    }
}
