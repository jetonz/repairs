﻿using System;
using System.Collections.Generic;

namespace WorkShop.Models.Entities
{
    public class Client : BaseEntity<Guid>
    {
        public Client()
        {
            this.Repairs = new HashSet<Repair>();
        }

        public string Name { get; set; }

        public bool IsExternal { get; set; }

        public virtual ICollection<Repair> Repairs { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}