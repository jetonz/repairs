﻿using WorkShop.Models.Entities;
using System;
using OfficeOpenXml;

namespace WorkShop.Models
{
    internal class ApplicationUser
    {
        private static ApplicationUser _instance;
        public User DbUser { get; private set; }


        private ApplicationUser(User dbUser)
        {
            DbUser = dbUser;
            
            //When user logged in we should init license for EPPlus
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
        }

        public static ApplicationUser GetInstance()
        {
            if (_instance == null)
            {
                throw new ArgumentNullException("User not specified");
            }
            return _instance;
        }

        public static void SetUser(User dbUser)
        {
            if (dbUser.Role == null)
            {
                throw new ArgumentException("User role is not provided");
            }

            if(dbUser.Role.ApplicationPermissions == null || dbUser.Role.RepairPermissions == null)
            {
                throw new ArgumentException("Role's premissions not provided");
            }

            _instance = new ApplicationUser(dbUser);
        }

    }
}
