﻿using System.Windows;

namespace WorkShop.Views.Menu.Breakages
{
    /// <summary>
    /// Interaction logic for BreakageList.xaml
    /// </summary>
    public partial class BreakagesListWindow : Window
    {
        public BreakagesListWindow()
        {
            InitializeComponent();
            BreakagesListViewModel VM = new BreakagesListViewModel();
            DataContext = VM;

            BtnDelete.Click += (s, e) =>
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Уверены что хотите удалить этот ремонт?", "", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    VM.DeleteSelectedItem.Execute(s);
                }
            };

            BtnCreate.Click += (s, e) =>
            {
                var newBreakage = new BreakageDetailsViewModel();
                newBreakage.OnModelSaved += () =>
                {
                    VM.UpdateTable();
                };
                var win = new BreakageDetailsWindow(newBreakage);
                win.Owner = this;

                win.Show();
            };

            BtnEdit.Click += (s, e) =>
            {
                var breakageToEdit = new BreakageDetailsViewModel(VM.SelectedBreakage.Id)
                {
                    Description = VM.SelectedBreakage.Description,
                    ExternalPrice = VM.SelectedBreakage.ExternalPrice,
                    InternalPrice = VM.SelectedBreakage.InternalPrice
                };

                breakageToEdit.OnModelSaved += () =>
                {
                    VM.UpdateTable();
                };

                var win = new BreakageDetailsWindow(breakageToEdit)
                {
                    Owner = this
                };
                win.Show();
            };

        }


    }
}
