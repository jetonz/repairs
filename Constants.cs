﻿using System;

namespace WorkShop
{
    public static class Constants
    {
        public static Guid InRepairStatusId => new Guid("2764510D-1870-41A1-8E88-54AB265EAD0E");
        public static Guid DefaultExternalClient => new Guid("87A4D40F-8555-4127-867F-E0C831942A45");
    }
}
