﻿using System;
using System.Collections.Generic;

namespace WorkShop.Models.Entities
{

    public class Status : BaseEntity<Guid>
    {
        public Status()
        {
            this.Repairs = new HashSet<Repair>();
            this.RepairMastersHistory = new HashSet<RepairMasterHistory>();
            this.RepairEditHistory = new HashSet<RepairEditHistory>();
        }

        public string Name { get; set; }
        public bool IsIssueStatus { get; set; } 

        public virtual ICollection<Repair> Repairs { get; set; }
        public virtual ICollection<RepairMasterHistory> RepairMastersHistory { get; set; }
        public virtual ICollection<RepairEditHistory> RepairEditHistory { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
