﻿using WorkShop.Models.Entities;
using System;
using System.Linq;
using System.Windows;

namespace WorkShop.Views.Menu.Clients
{
    /// <summary>
    /// Interaction logic for MastersListView.xaml
    /// </summary>
    public partial class ClientsListWindow : Window
    {
        public ClientsListWindow()
        {
            InitializeComponent();
            ClientsListViewModel VM = new ClientsListViewModel();
            this.DataContext = VM;

            this.Edit.Click += (s, e) =>
            {
                if (VM.CurrentClient != null)
                {
                    DialogWindow win = new DialogWindow();
                    win.Val.Text = VM.CurrentClient.Name;
                    win.Title = "Новое имя контрагента";
                    win.ShowDialog();

                    while (win.IsVisible)
                    {

                    }
                    VM.CurrentClient.Name = win.Val.Text;
                    VM.ClientsList.Where(x => x.Id == VM.CurrentClient.Id).FirstOrDefault().Name = win.Val.Text;
                    VM.EditClient();
                }
            };

            this.Create.Click += (s, e) =>
            {
                DialogWindow win = new DialogWindow();
                win.Title = "Создать контрагента";
                win.ShowDialog();
                while (win.IsVisible)
                {

                }
                if (!String.IsNullOrEmpty(win.Val.Text))
                {
                    VM.CreateClient(win.Val.Text);
                }

            };

            this.Delete.Click += (s, e) =>
            {
                if (VM.CurrentClient != null)
                {
                    VM.DeleteClient(VM.CurrentClient);
                }
            };

            this.ClientsList.SelectionChanged += (s, e) =>
            {
                VM.CurrentClient = (Client)this.ClientsList.SelectedItem;
            };


        }
    }
}
