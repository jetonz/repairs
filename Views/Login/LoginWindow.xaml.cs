﻿using WorkShop.Controls;
using System.Windows;
using System.Security;
using WorkShop.Views.Repairs;

namespace WorkShop.Views.Login
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginWindow : Window, IHavePassword
    {
        public LoginWindow()
        {
            InitializeComponent();
            LoginViewModel VM = new LoginViewModel();
            this.DataContext = VM;

            VM.OnCloseView += () =>
            {
                LoginWindow main = Application.Current.MainWindow as LoginWindow;
                if (main != null)
                {
                    RepairsListWindow newWindow = new RepairsListWindow();
                    newWindow.Show();
                    main.Close();
                }
            };

            if (VM.IsFirstVisit)
            {
                this.ToDo.Content = "Создать";
            }
        }

        public SecureString Password
        {
            get
            {
                return this.Pass.SecurePassword;
            }
        }
    }
}
