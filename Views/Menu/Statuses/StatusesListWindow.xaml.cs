﻿using WorkShop.Models.Entities;
using System;
using System.Linq;
using System.Windows;

namespace WorkShop.Views.Menu.Statuses
{
    /// <summary>
    /// Interaction logic for StatusList.xaml
    /// </summary>
    public partial class StatusesListWindow : Window
    {
        public StatusesListWindow()
        {
            StatusesListViewModel VM = new StatusesListViewModel();
            InitializeComponent();
            this.DataContext = VM;

            this.Statuses.SelectionChanged += (s, e) =>
            {
                VM.CurrStatus = (Status)this.Statuses.SelectedItem;
            };

            this.Edit.Click += (s, e) =>
            {
                if (VM.CurrStatus != null)
                {
                    DialogWindow win = new DialogWindow();
                    win.Val.Text = VM.CurrStatus.Name;
                    win.Title = "Новое название статуса";
                    win.PanelForIssued.Visibility = Visibility.Visible;
                    win.IsIssuedValue.IsChecked = VM.CurrStatus.IsIssueStatus; 
                    win.ShowDialog();

                    while (win.IsVisible)
                    {

                    }
                    VM.CurrStatus.Name = win.Val.Text;
                    VM.CurrStatus.IsIssueStatus = (bool)win.IsIssuedValue.IsChecked; 
                    VM.StatusesList.Where(x => x.Id == VM.CurrStatus.Id).FirstOrDefault().Name = win.Val.Text;
                    VM.EditStatus();
                }
            };

            this.Create.Click += (s, e) =>
            {
                DialogWindow win = new DialogWindow();
                win.Title = "Создать статус";
                win.PanelForIssued.Visibility = Visibility.Visible;
                win.ShowDialog();
                while (win.IsVisible)
                {

                }
                if (!String.IsNullOrEmpty(win.Val.Text))
                {
                    VM.CreateStatus(win.Val.Text, (bool)win.IsIssuedValue.IsChecked);
                }
            };

            this.Delete.Click += (s, e) =>
            {
                if(VM.CurrStatus != null)
                {
                    VM.DeleteStatus(VM.CurrStatus);
                }
            };
        }
    }
}
