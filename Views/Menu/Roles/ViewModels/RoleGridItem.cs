﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkShop.Models.Entities;

namespace WorkShop.Views.Menu.Roles
{
    public class RoleGridItem
    {
        public string UsersList => string.Join(", ", Role.Users.Select(x=> x.Name));

        public string RoleName => Role.Name;

        public Role Role { get; }

        public RoleGridItem(Role role)
        {
            this.Role = role;
        }

       
    }
}
