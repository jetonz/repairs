﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using WorkShop.Database;
using WorkShop.Libs;
using WorkShop.ViewModels;

namespace WorkShop.Views.Menu.Breakages
{
    public class BreakagesListViewModel : BaseViewModel
    {
        readonly RepairsContext _dbContext;

        private List<BreakageDetailsViewModel> _breakages;
        public IReadOnlyCollection<BreakageDetailsViewModel> Breakages => _breakages.AsReadOnly();

        private BreakageDetailsViewModel _selectedBreakage;
        public BreakageDetailsViewModel SelectedBreakage
        {
            get => _selectedBreakage;
            set
            {
                _selectedBreakage = value;
                OnPropertyChanged(nameof(SelectedBreakage));
            }
        } 

        public ICommand DeleteSelectedItem { get; set; }

        public BreakagesListViewModel()
        {
            _dbContext = ContextFactory.CreateContext();
            UpdateTable(); 
            DeleteSelectedItem = new RelayCommand(DeleteSelectedBreakage);
        }

        public void UpdateTable()
        {
            _breakages = _dbContext.Breakages
                .AsNoTracking()
                .Where(x=> !x.IsDeleted)
                .OrderBy(x => x.Description)
                .ToArray()
                .Select(x => new BreakageDetailsViewModel(x.Id)
                {
                    Description = x.Description,
                    ExternalPrice = x.ExternalPrice,
                    InternalPrice = x.InternalPrice
                }).ToList();

            OnPropertyChanged(nameof(Breakages));
        }

        private void DeleteSelectedBreakage(object sender)
        {
            if (SelectedBreakage == null)
            {
                return;
            }

            var dbBreakage = _dbContext.Breakages.Find( SelectedBreakage.Id);
            dbBreakage.IsDeleted = true;
            _dbContext.SaveChanges();
            UpdateTable();
        }


        ~BreakagesListViewModel()
        {
            _dbContext.Dispose();
        }
    }
}
