﻿using WorkShop.Database;
using WorkShop.Libs;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Windows.Input;
using System.ComponentModel;

namespace WorkShop.Views.Menu.Roles
{
    public class RoleDetailsViewModel : BaseViewModel, IDataErrorInfo
    {
        private readonly RepairsContext _context;
        private readonly Role _role;



        public string Name
        {
            get => _role.Name;
            set
            {
                _role.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }


        public bool CanViewAllRepairs
        {
            get => _role.RepairPermissions.CanViewAllRepairs;
            set
            {
                _role.RepairPermissions.CanViewAllRepairs = value;
                OnPropertyChanged(nameof(CanViewAllRepairs));
            }
        }
        public bool CanCreateRepair
        {
            get => _role.RepairPermissions.CanCreateRepair;
            set
            {
                _role.RepairPermissions.CanCreateRepair = value;
                OnPropertyChanged(nameof(CanCreateRepair));
            }
        }
        public bool CanEditAnyRepair
        {
            get => _role.RepairPermissions.CanEditAnyRepair;
            set
            {
                _role.RepairPermissions.CanEditAnyRepair = value;
                OnPropertyChanged(nameof(CanEditAnyRepair));
            }
        }
     
        public bool CanSeePhone
        {
            get => _role.RepairPermissions.CanSeePhone;
            set
            {
                _role.RepairPermissions.CanSeePhone = value;
                OnPropertyChanged(nameof(CanSeePhone));
            }
        }

        public bool CanAddClients
        {
            get => _role.RepairPermissions.CanAddClients;
            set
            {
                _role.RepairPermissions.CanAddClients = value;
                OnPropertyChanged(nameof(CanAddClients));
            }
        }
     
        public bool CanSeeMasters
        {
            get => _role.RepairPermissions.CanSeeMasters;
            set
            {
                _role.RepairPermissions.CanSeeMasters = value;
                OnPropertyChanged(nameof(CanSeeMasters));
            }
        }
        public bool CanSeeStatus
        {
            get => _role.RepairPermissions.CanSeeStatus;
            set
            {
                _role.RepairPermissions.CanSeeStatus = value;
                OnPropertyChanged(nameof(CanSeeStatus));
            }
        }
        public bool CanSeePriority
        {
            get => _role.RepairPermissions.CanSeePriority;
            set
            {
                _role.RepairPermissions.CanSeePriority = value;
                OnPropertyChanged(nameof(CanSeePriority));
            }
        }
        public bool CanSeeAccepter
        {
            get => _role.RepairPermissions.CanSeeAccepter;
            set
            {
                _role.RepairPermissions.CanSeeAccepter = value;
                OnPropertyChanged(nameof(CanSeeAccepter));
            }
        }
        public bool CanSeePrice
        {
            get => _role.RepairPermissions.CanSeePrice;
            set
            {
                _role.RepairPermissions.CanSeePrice = value;
                OnPropertyChanged(nameof(CanSeePrice));
            }
        }
        public bool CanSeeSelfPrices
        {
            get => _role.RepairPermissions.CanSeeSelfPrices;
            set
            {
                _role.RepairPermissions.CanSeeSelfPrices = value;
                OnPropertyChanged(nameof(CanSeeSelfPrices));
            }
        }
        public bool CanSeeReason
        {
            get => _role.RepairPermissions.CanSeeReason;
            set
            {
                _role.RepairPermissions.CanSeeReason = value;
                OnPropertyChanged(nameof(CanSeeReason));
            }
        }
        public bool CanSeeHistory
        {
            get => _role.RepairPermissions.CanSeeHistory;
            set
            {
                _role.RepairPermissions.CanSeeHistory = value;
                OnPropertyChanged(nameof(CanSeeHistory));
            }
        }
        public bool CanAgreePrice
        {
            get => _role.RepairPermissions.CanAgreePrice;
            set
            {
                _role.RepairPermissions.CanAgreePrice = value;
                OnPropertyChanged(nameof(CanAgreePrice));
            }
        }

        public bool CanEditIssuedRepairs
        {
            get => _role.RepairPermissions.CanEditIssuedRepairs;
            set
            {
                _role.RepairPermissions.CanEditIssuedRepairs = value;
                OnPropertyChanged(nameof(CanEditIssuedRepairs));
            }
        }


        public bool HasUsersAccess
        {
            get => _role.ApplicationPermissions.HasUsersAccess;
            set
            {
                _role.ApplicationPermissions.HasUsersAccess = value;
                OnPropertyChanged(nameof(HasUsersAccess));
            }
        }
        public bool HasMastersAccess
        {
            get => _role.ApplicationPermissions.HasMastersAccess;
            set
            {
                _role.ApplicationPermissions.HasMastersAccess = value;
                OnPropertyChanged(nameof(HasMastersAccess));
            }
        }
        public bool HasReportsAccess
        {
            get => _role.ApplicationPermissions.HasReportsAccess;
            set
            {
                _role.ApplicationPermissions.HasReportsAccess = value;
                OnPropertyChanged(nameof(HasReportsAccess));
            }
        }
        public bool HasStatusesAccess
        {
            get => _role.ApplicationPermissions.HasStatusesAccess;
            set
            {
                _role.ApplicationPermissions.HasStatusesAccess = value;
                OnPropertyChanged(nameof(HasStatusesAccess));
            }
        }
        public bool HasRolesAccess
        {
            get => _role.ApplicationPermissions.HasRolesAccess;
            set
            {
                _role.ApplicationPermissions.HasRolesAccess = value;
                OnPropertyChanged(nameof(HasRolesAccess));
            }
        }
        public bool HasClientsAccess
        {
            get => _role.ApplicationPermissions.HasClientsAccess;
            set
            {
                _role.ApplicationPermissions.HasClientsAccess = value;
                OnPropertyChanged(nameof(HasClientsAccess));
            }
        }
        public bool HasBreakagesAccess
        {
            get => _role.ApplicationPermissions.HasBreakagesAccess;
            set
            {
                _role.ApplicationPermissions.HasBreakagesAccess = value;
                OnPropertyChanged(nameof(HasBreakagesAccess));
            }
        }

        public string Error { get; private set; }

        public string this[string columnName]
        {
            get
            {
                Error = string.Empty;
                switch (columnName)
                {
                    case nameof(Name):
                        if (string.IsNullOrEmpty(Name))
                        {
                            Error = "Укажите название роли";
                        }
                        break;
                }

                return Error;
            }
        }

        public RoleDetailsViewModel()
        {
            _context = ContextFactory.CreateContext();
            _role = new Role
            {
                ApplicationPermissions = new ApplicationPermissions(),
                RepairPermissions = new RepairPermissions()
            };
        }

        public RoleDetailsViewModel(Guid roleId)
        {
            _context = ContextFactory.CreateContext();
            _role = _context.Roles.Find(roleId);
        }


        public void Save()
        {
            if (!string.IsNullOrEmpty(Error))
            {
                return;
            }

            if (_role.Id == Guid.Empty)
            {
                _role.Id = Guid.NewGuid();
                _context.Roles.Add(_role);
            }

            _context.SaveChanges();

        }


        ~RoleDetailsViewModel()
        {
            _context.Dispose();
        }
    }
}
