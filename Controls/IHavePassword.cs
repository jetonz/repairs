﻿namespace WorkShop.Controls
{
    interface IHavePassword
    {
        System.Security.SecureString Password { get; }
    }
}
