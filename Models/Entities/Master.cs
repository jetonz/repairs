﻿using System;
using System.Collections.Generic;

namespace WorkShop.Models.Entities
{
    public class Master : BaseEntity<Guid>
    {
        public Master()
        {
            this.Repairs = new HashSet<Repair>();
            this.RepairMastersHistory = new HashSet<RepairMasterHistory>();
        }

        public string Name { get; set; }

        public virtual ICollection<Repair> Repairs { get; set; }

        public virtual ICollection<RepairMasterHistory> RepairMastersHistory { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}