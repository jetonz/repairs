using System.Linq;
using OfficeOpenXml;
using WorkShop.Models.Entities;

namespace WorkShop.Helpers
{
    public static class ExcelTemplateConstants
    {
        public const string REPAIR_ID = "REPAIR_ID";
        public const string REPAIR_FIO = "REPAIR_FIO";
        public const string REPAIR_PHONE = "REPAIR_PHONE";
        public const string REPAIR_MODEL = "REPAIR_MODEL";
        public const string REPAIR_IMEI = "REPAIR_IMEI";
        public const string REPAIR_DEFFECT = "REPAIR_DEFFECT";
        public const string REPAIR_PHONEDESC = "REPAIR_PHONEDESC";
        public const string REPAIR_CREATED_AT = "REPAIR_CREATED_AT";
        public const string REPAIR_ACCEPTED_BY = "REPAIR_ACCEPTED_BY";
    }



    public static class EPPlusHelper
    {
        public static void ReplaceMarkers(ExcelWorksheet excelWorksheet, Repair repair)
        {
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_ID, repair.Id.ToString());
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_FIO, repair.FIO);
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_PHONEDESC, repair.PhoneDesc);
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_PHONE, repair.Phone);
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_MODEL, repair.Model);
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_IMEI, repair.Imei);
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_DEFFECT, repair.Deffect);
            excelWorksheet.Replace(ExcelTemplateConstants.REPAIR_CREATED_AT, repair.CreatedAt.ToString("dd.MM.yyyy"));
        }


        public static void Replace(this ExcelWorksheet ws, string valueToSearch, string valueToReplace)
        {
            // search in all cells
            // https://github.com/JanKallman/EPPlus/wiki/Addressing-a-worksheet
            var query = from cell in ws.Cells["A:XFD"] 
                where cell.Value?.ToString().Contains(valueToSearch) == true
                select cell;

            foreach(var cell in query)
            {
                cell.Value = cell.Value.ToString().Replace(valueToSearch, valueToReplace ?? "");
            }
        }


        public static void AdjustPrintSettings(this ExcelWorksheet ws)
        {
            ws.PrinterSettings.FitToPage = true;
            ws.PrinterSettings.FitToHeight = 150;
            ws.PrinterSettings.Orientation = eOrientation.Landscape;
        }
    }
    
    
}