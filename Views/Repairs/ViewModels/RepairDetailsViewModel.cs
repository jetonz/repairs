﻿using WorkShop.Database;
using WorkShop.Models;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.IO.Packaging;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Xps.Packaging;
using OfficeOpenXml;
using WorkShop.Helpers;
using PrintDialog = System.Windows.Controls.PrintDialog;

namespace WorkShop.Views.Repairs
{
    public class RepairDetailsViewModel : BaseViewModel, IDataErrorInfo
    {
        #region Vars

        private readonly User _currApplicationUser;
        private readonly RepairsContext _context;

        private Repair _repair;
        private bool _masterWasUpdated;
        private bool _priceWasAgreed;

        public bool CanAddClients => _currApplicationUser.Role.RepairPermissions.CanAddClients;

        public bool CanSeeMasters => _currApplicationUser.Role.RepairPermissions.CanSeeMasters;

        public bool CanSeeStatus => _currApplicationUser.Role.RepairPermissions.CanSeeStatus;

        public bool CanSeePriority => _currApplicationUser.Role.RepairPermissions.CanSeePriority;

        public bool CanSeeAccepter => _currApplicationUser.Role.RepairPermissions.CanSeeAccepter;

        public bool CanSeePrice => _currApplicationUser.Role.RepairPermissions.CanSeePrice;

        public bool CanSeeReason => _currApplicationUser.Role.RepairPermissions.CanSeeReason;

        public bool CanSeeHistory => _currApplicationUser.Role.RepairPermissions.CanSeeHistory;

        public bool CanAgreePrice => this.CanEditFields && _currApplicationUser.Role.RepairPermissions.CanAgreePrice;

        public bool CanSeePhone => IsCreatedByCurrentUser || _currApplicationUser.Role.RepairPermissions.CanSeePhone;

        public bool CanSeeSelfPrice => _currApplicationUser.Role.RepairPermissions.CanSeeSelfPrices;

        public bool CanEditFields
        {
            get
            {
                // no way to edit issued repairs
                if (_repair.DateIsued.HasValue)
                {
                    return _currApplicationUser.Role.RepairPermissions.CanEditIssuedRepairs;
                }

                //has permission to edit anything
                if (_currApplicationUser.Role.RepairPermissions.CanEditAnyRepair)
                {
                    return true;
                }

                //new repair
                if (_currApplicationUser.Role.RepairPermissions.CanCreateRepair && this._repair.Id == 0)
                {
                    return true;
                }

                //this is owned of the repair
                return this._repair.CreatedBy.Id == _currApplicationUser.Id && (Accepter == null || Accepter.Id == _currApplicationUser.Id);
            }
        }


        public bool IsCreatedByCurrentUser => this._repair.CreatedBy.Id == _currApplicationUser.Id;

        public bool CanChangeAccepter => this._repair.Accepter == null || this._repair.Accepter.Id == _currApplicationUser.Id;

        public bool CanSaveRepair => CanEditFields;


        private string _error;

        public string Error
        {
            get { return _error; }
            set
            {
                _error = value;
                OnPropertyChanged(nameof(Error));
            }
        }


        private Collection<string> _repairMastersHistory;

        public IReadOnlyCollection<string> RepairMastersHistory
        {
            get
            {
                if (_repairMastersHistory != null)
                {
                    return _repairMastersHistory;
                }

                _repairMastersHistory = new Collection<string>();

                foreach (var item in _repair.RepairMastersHistory)
                {
                    string status = item.Status?.Name ?? "неопределено";
                    _repairMastersHistory.Add(String.Format("{1} {0} ({2})", item.Master.Name, item.DateChanged.ToShortDateString(), status));
                }

                return _repairMastersHistory;
            }
        }

        private Collection<string> _repairEditsHistory;

        public IReadOnlyCollection<string> RepairEditsHistory
        {
            get
            {
                if (_repairEditsHistory != null)
                {
                    return _repairEditsHistory;
                }

                _repairEditsHistory = new Collection<string>(
                    _repair.RepairEditHistory.Select(x =>
                        {
                            if (x.Action == ERepairHistoryAction.UpdatedStatus)
                            {
                                return String.Format("{1} {0} ({2})",
                                    x.User.Name,
                                    x.CreatedAt.ToLocalTime().ToShortDateString(),
                                    x.Status?.Name ?? "без статуса");
                            }
                            else if (x.Action == ERepairHistoryAction.PriceAgreed)
                            {
                                return $"{x.CreatedAt.ToLocalTime().ToShortDateString()} {x.User.Name} согласовал(а) цену";
                            }
                            else
                            {
                                return "";
                            }
                        })
                        .ToList()
                );

                return _repairEditsHistory;
            }
        }

        public ObservableCollection<Client> AllClients { get; private set; }

        public List<Master> AllMasters { get; private set; }

        public List<Status> AllStatuses { get; private set; }

        public List<User> AllUsers { get; private set; }

        public List<Breakage> AllBreakages { get; private set; }


        public int Id => _repair.Id;

        public bool IsCheked => _repair.IsPayed;

        public decimal SelfPrice => _repair.SelfPrice;

        public decimal AdditionalIncome => _repair.AdditionalIncome;

        public bool PriceNotAgreed => _repair.PriceAgreedBy != null
            ? false
            : true;

        public ERepairUrgency UrgencyEnum
        {
            get => _repair.Urgency;
            set
            {
                _repair.Urgency = value;
                OnPropertyChanged(nameof(UrgencyEnum));
            }
        }

        public string PhoneDescription
        {
            get => _repair.PhoneDesc;
            set => _repair.PhoneDesc = value;
        }

        public string FIO
        {
            get => _repair.FIO;
            set => _repair.FIO = value;
        }

        public string IMEI
        {
            get => _repair.Imei;
            set
            {
                _repair.Imei = value;
                Error = null;
                OnPropertyChanged(nameof(IMEI));
            }
        }

        public string Phone
        {
            get => _repair.Phone;
            set
            {
                _repair.Phone = value;
                OnPropertyChanged(nameof(Phone));
            }
        }

        public string Model
        {
            get => _repair.Model;
            set
            {
                _repair.Model = value;
                Error = null;
                OnPropertyChanged(nameof(Model));
            }
        }

        public string Deffect
        {
            get => _repair.Deffect;
            set
            {
                _repair.Deffect = value;
                OnPropertyChanged(nameof(Deffect));
            }
        }

        public string Reason
        {
            get => _repair.Reason;
            set
            {
                _repair.Reason = value;
                OnPropertyChanged(nameof(Reason));
            }
        }

        public decimal Price
        {
            get => _repair.Price;
            set
            {
                _repair.Price = value;
                OnPropertyChanged(nameof(SelfPrice));
                OnPropertyChanged(nameof(AdditionalIncome));
                OnPropertyChanged(nameof(Price));
            }
        }

        public decimal SuppliesPrice
        {
            get => _repair.SuppliesPrice;
            set
            {
                _repair.SuppliesPrice = value;
                OnPropertyChanged(nameof(SelfPrice));
                OnPropertyChanged(nameof(AdditionalIncome));
                OnPropertyChanged(nameof(SuppliesPrice));
            }
        }

        public decimal WorkPrice
        {
            get => _repair.WorkPrice;
            set
            {
                _repair.WorkPrice = value;
                OnPropertyChanged(nameof(SelfPrice));
                OnPropertyChanged(nameof(WorkPrice));
                OnPropertyChanged(nameof(AdditionalIncome));
            }
        }

        public string InvoiceNumber
        {
            get => _repair.InvoiceNumber;
            set
            {
                _repair.InvoiceNumber = value;
                OnPropertyChanged(nameof(InvoiceNumber));
            }
        }

        public string Comment
        {
            get => _repair.Comment;
            set
            {
                _repair.Comment = value;
                OnPropertyChanged(nameof(Comment));
            }
        }


        public Client Client
        {
            get { return _repair.Client; }
            set
            {
                _repair.SetClient(value);
                Error = null;
                if (Breakage != null)
                {
                    if (value.IsExternal)
                    {
                        this.WorkPrice = Breakage.ExternalPrice;
                    }
                    else
                    {
                        this.WorkPrice = Breakage.InternalPrice;
                    }
                }

                OnPropertyChanged(nameof(Client));
            }
        }

        public Master Master
        {
            get => _repair.Master;
            set
            {
                _repair.Master = value;
                _masterWasUpdated = true;
            }
        }

        public Status Status
        {
            get => _repair.Status;
            set { _repair.Status = value; }
        }

        public User Accepter
        {
            get => _repair.Accepter;
            set => _repair.Accepter = value;
        }

        public Breakage Breakage
        {
            get => _repair.Breakage;
            set
            {
                _repair.Breakage = value;
                if (this.Client == null)
                {
                    return;
                }

                if (this.Client.IsExternal)
                {
                    this.WorkPrice = value.ExternalPrice;
                }
                else
                {
                    this.WorkPrice = value.InternalPrice;
                }
            }
        }


        public string this[string columnName]
        {
            get
            {
                string err = null;

                switch (columnName)
                {
                    case nameof(IMEI):
                        if (String.IsNullOrEmpty(IMEI))
                        {
                            err = "Нужен IMEI";
                        }

                        break;
                    case nameof(Model):
                        if (String.IsNullOrEmpty(Model))
                        {
                            err = "Не указана модель";
                        }

                        break;
                    case nameof(Client):
                        if (Client == null)
                        {
                            err = "Не указан владелец!";
                        }

                        break;
                }

                return err;
            }
        }

        #endregion



        public RepairDetailsViewModel()
        {
            _currApplicationUser = ApplicationUser.GetInstance().DbUser;
            _context = ContextFactory.CreateContext();
            _repair = new Repair(_context.Users.Find(_currApplicationUser.Id));
            InitListValues();
            if (CanSeeAccepter)
            {
                Accepter = _currApplicationUser;
            }
        }


        public RepairDetailsViewModel(int repairId)
        {
            _context = ContextFactory.CreateContext();
            LoadRepair(repairId);
            InitListValues();

            _currApplicationUser = ApplicationUser.GetInstance().DbUser;
            if (_repair.Urgency == ERepairUrgency.StreetRepair)
                _repair.Urgency = ERepairUrgency.DefaulRepair;
        }


        private void LoadRepair(int id)
        {
            _repair = _context.Repairs
                .Include(nameof(Repair.Master))
                .Include(nameof(Repair.Client))
                .Include(nameof(Repair.Accepter))
                .Include(nameof(Repair.Status))
                .Include(nameof(Repair.Breakage))
                .Include(nameof(Repair.PriceAgreedBy))
                .Include(nameof(Repair.RepairEditHistory))
                .Include($"{nameof(Repair.RepairEditHistory)}.{nameof(RepairEditHistory.Status)}")
                .Include($"{nameof(Repair.RepairEditHistory)}.{nameof(RepairEditHistory.User)}")
                .Include($"{nameof(Repair.RepairMastersHistory)}.{nameof(RepairMasterHistory.Master)}")
                .Include($"{nameof(Repair.RepairMastersHistory)}.{nameof(RepairMasterHistory.Status)}")
                .First(x => x.Id == id);
        }


        private void InitListValues()
        {
            AllMasters = _context.Masters.OrderBy(x => x.Name).ToList();
            AllClients = new ObservableCollection<Client>(_context.Clients.OrderBy(x => x.Name).ToList());
            AllStatuses = _context.Statuses.OrderBy(x => x.Name).ToList();
            AllUsers = _context.Users.ToList();
            AllBreakages = _context.Breakages.Where(x => !x.IsDeleted).OrderBy(x => x.Description).ToList();
        }


        public void CreateClient(string clientName)
        {
            Client client = new Client
            {
                Name = clientName,
                Id = Guid.NewGuid()
            };

            _context.Clients.Add(client);
            _context.SaveChanges();

            AllClients.Add(client);
            Client = client;
        }


        public void SetPriceAgreed()
        {
            _repair.PriceAgreedBy = AllUsers.First(x => x.Id == _currApplicationUser.Id);
            _priceWasAgreed = true;
            OnPropertyChanged(nameof(PriceNotAgreed));
        }


        public void SaveRepair()
        {
            if (String.IsNullOrEmpty(IMEI))
            {
                Error = "Нужен IMEI";
                return;
            }

            if (String.IsNullOrEmpty(Model))
            {
                Error = "Не указана модель";
                return;
            }

            if (Client == null)
            {
                Error = "Не указан владелец!";
                return;
            }

            _repair.LastEdited = _currApplicationUser.Id;
            _repair.UpdatedAt = DateTime.Now;

            //add master to history if he assigned      
            if (_masterWasUpdated)
            {
                _repair.RepairMastersHistory.Add(new RepairMasterHistory
                {
                    DateChanged = DateTime.Now,
                    MasterId = _repair.Master?.Id,
                    RepairId = _repair.Id,
                    StatusId = _repair.Status?.Id
                });
            }

            if (_priceWasAgreed)
            {
                var priceAgreedHistoryItem = new RepairEditHistory
                {
                    RepairId = _repair.Id,
                    UserId = _currApplicationUser.Id,
                    CreatedAt = DateTime.UtcNow,
                    Action = ERepairHistoryAction.PriceAgreed
                };

                _repair.RepairEditHistory.Add(priceAgreedHistoryItem);
            }

            //add to edit history those, who created/updated this repair           
            _repair.RepairEditHistory
                .Add(
                    new RepairEditHistory
                    {
                        StatusId = _repair.Status?.Id,
                        RepairId = _repair.Id,
                        UserId = _currApplicationUser.Id,
                        CreatedAt = DateTime.UtcNow,
                        Action = ERepairHistoryAction.UpdatedStatus
                    }
                );

            SaveRepairToDatabase();
        }


        private void SaveRepairToDatabase()
        {
            try
            {
                if (_repair.Id == 0)
                {
                    _context.Repairs.Add(_repair);
                }

                if (_repair.PriceAgreedBy != null)
                {
                    _context.Set<User>().Attach(_repair.PriceAgreedBy);
                }

                _context.SaveChanges();
                Error = String.Empty;
            }
            catch (Exception e)
            {
                Error = e.Message; //"Ошибка записи в БД!";
            }
        }


        public void PrintRepair()
        {
            const string templateName = "template.xlsx";
            if (!File.Exists(templateName))
            {
                Error = "Файл шаблона не найден";
                return;
            }

            try
            {
                string resultFilePath = null;

                using (FileStream source = System.IO.File.OpenRead(templateName))
                using (ExcelPackage excelPackage = new OfficeOpenXml.ExcelPackage(source))
                {
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();
                    EPPlusHelper.ReplaceMarkers(worksheet, _repair);
                    worksheet.Replace(ExcelTemplateConstants.REPAIR_ACCEPTED_BY, ApplicationUser.GetInstance().DbUser.Name);
                    worksheet.AdjustPrintSettings();

                    resultFilePath = Path.Combine(Path.GetTempPath(), $"{DateTime.Now.ToString("yyMMdd_hhmmss")}.xlsx");
                    excelPackage.SaveAs(resultFilePath);
                }
                
                if (resultFilePath != null)
                {
                    System.Diagnostics.Process.Start(resultFilePath);
                }
                else
                {
                    Error = "Не вдалося відкрити файл квитанції";
                }
            }
            catch (Exception e)
            {
                Error = e.Message;
            }
        }


        ~RepairDetailsViewModel()
        {
            _context.Dispose();
        }
    }
}