﻿using WorkShop.Views.Menu.Breakages;
using WorkShop.Views.Menu.Clients;
using WorkShop.Views.Menu.Masters;
using WorkShop.Views.Menu.Roles;
using WorkShop.Views.Menu.Statuses;
using WorkShop.Views.Menu.Users;
using System;
using System.Windows;
using WorkShop.Views.Menu.Reports;

namespace WorkShop.Views.Repairs
{
   
    public partial class RepairsListWindow : Window
    {
        private readonly RepairsListViewModel VM;

        public RepairsListWindow()
        {
            this.VM = new RepairsListViewModel();

            InitializeComponent();
            this.DataContext = VM;

            this.masterList.ItemsSource = VM.MastersFilterList; 
            this.statusList.ItemsSource = VM.StatusesFilterList;
            this.acceptersList.ItemsSource = VM.AcceptersFilterList; 

            VM.OpenWindowHandler += (DataContext) =>
            {
                RepairDetailsWindow DialogWin = new RepairDetailsWindow(DataContext);
                DialogWin.Show();
                DialogWin.Closed += (s, e) =>
                {
                    VM.LoadRepairList(NavigationOperation.REFRESH);
                };
            };

            string NewTitle = String.Format("Ремонты ({0})", VM.CueerntUserName);
            this.Title = NewTitle;

            BindButtons();
        }

        private void BindButtons()
        {
            this.ExitMenuItem.Click += (s, e) =>
            {
                this.Close();
            };

            this.EditUser.Click += (s, e) =>
            {
                UsersListWindow win = new UsersListWindow();
                win.Show();
            };

            this.EditStatus.Click += (s, e) =>
            {
                StatusesListWindow win = new StatusesListWindow();
                win.Show();
            };

            this.EditMaster.Click += (s, e) =>
            {
                MastersListWindow win = new MastersListWindow();
                win.Show();
            };

            this.EditRoles.Click += (s, e) =>
            {
                RolesListWindow win = new RolesListWindow();
                win.Show();
            };

            this.EditClients.Click += (s, e) =>
            {
                ClientsListWindow win = new ClientsListWindow();
                win.Show();
            };

            this.EditReports.Click += (s, e) =>
            {
                ReportWindow win = new ReportWindow();
                win.Show();
            };

            this.EditBreakages.Click += (s, e) =>
            {
                BreakagesListWindow win = new BreakagesListWindow();
                win.Show();
            };

            this.Settings.Click += (s, e) =>
            {
                MessageBoxResult result = MessageBox.Show("Maybe later, baby", "-__-", MessageBoxButton.OK, MessageBoxImage.Question);
            };

            this.ResetFilterBtn.Click += (s, e) =>
            {
                VM.ResetFilters();
            };

            this.NotificationBtn.Click += (s, e) =>
            {
                VM.IsWarningFiltered = !VM.IsWarningFiltered;
            };

            this.NextButton.Click += (s, e) =>
            {
                VM.LoadRepairList(NavigationOperation.NEXT, delay: 100);
            };

            this.PrevButton.Click += (s, e) =>
            {
                VM.LoadRepairList(NavigationOperation.PREV, delay: 100);
            };

            this.RefreshTable.Click += (s, e) =>
            {
                VM.LoadRepairList(NavigationOperation.REFRESH);
            };
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }
    }
}
