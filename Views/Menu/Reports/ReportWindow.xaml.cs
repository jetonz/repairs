﻿using System.Windows;

namespace WorkShop.Views.Menu.Reports
{
    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {

        public ReportWindow()
        {
            ReportViewModel VM = new ReportViewModel();
            InitializeComponent();
            this.DataContext = VM;

            this.ReportFieldsList.SelectionChanged += (s, e) =>
            {
                int item = this.ReportFieldsList.SelectedIndex;

                switch (item)
                {
                    case (0):
                        this.ReportValuesList.ItemsSource = VM.MastersFields;
                        break;
                    case (1):
                        this.ReportValuesList.ItemsSource = VM.StatusesFields;
                        break;
                    case (2):
                        this.ReportValuesList.ItemsSource = VM.ClientsFields;
                        break;
                    case (3):
                        this.ReportValuesList.ItemsSource = VM.AcceptersFields;
                        break;
                }
            };

            VM.EventCheckoutRepairs += () =>
            {             

                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(this, "Посчитать эти ремонты?", "Посчитать?" , System.Windows.MessageBoxButton.YesNo);

                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            };

            this.ReportValuesList.MouseDoubleClick += (s, e) =>
            {
                VM.AddItemToReport(this.ReportValuesList.SelectedItem);
            };

            this.ReportValuesListView.MouseDoubleClick += (s, e) =>
            {
                if (this.ReportValuesListView.SelectedIndex >= 0)
                {
                    VM.RemoveItemFromRepor(this.ReportValuesListView.SelectedIndex);
                }
            };

            this.ButtonCancle.Click += (s, e) =>
            {
                this.Close();
            };

            this.ButtonOk.Click += (s, e) =>
            {
                VM.BuildReport();
            };

        }
    }
}
