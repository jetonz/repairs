﻿using WorkShop.Controls;
using WorkShop.Database;
using WorkShop.Libs;
using WorkShop.Models;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Data.Entity;
using System.Linq;
using System.Windows.Input;

namespace WorkShop.Views.Login
{

    public class LoginViewModel : BaseViewModel
    {
        private RepairsContext DbContext;

        public string Password { get; set; }

        private string _login;
        public string Login
        {
            get
            {
                return _login;
            }
            set
            {
                _login = value.ToLower();
            }
        }
        public bool IsFirstVisit { get; set; }

        public Action OnCloseView;

        private string _error;
        public string Error
        {
            get
            {
                return _error;
            }
            set
            {
                _error = value;
                OnPropertyChanged("Error");
            }
        }

        public ICommand Action { get; private set; }

        public LoginViewModel()
        {
            try
            {
                DbContext = ContextFactory.CreateContext();
                IsNewUser();
            }
            catch (Exception e)
            {
                if (e.HResult == -2146233087)
                {
                    Error = "Не удалось соединиться с БД";
                }
                else
                {
                    Error = e.Message;
                }
            }

            if (IsFirstVisit)
            {
                Action = new RelayCommand(RegisterNewUser);
            }
            else
            {
                Action = new RelayCommand(DoLogin);
            }

        }

        private void RegisterNewUser(object parameter)
        {
            var passwordContainer = parameter as IHavePassword;
            if (passwordContainer != null)
            {
                var secureString = passwordContainer.Password;
                Password = ConvertToUnsecureString(secureString);
            }
            else
            {
                Error = "Пустой пароль!";
                return;
            }

            if (String.IsNullOrEmpty(Login) || String.IsNullOrEmpty(Password))
            {
                Error = "Пустой логин или пароль!";
                return;
            }

            string pass = PasswordMaker.CreatMd5(Password);

            Role role = new Role
            {
                RepairPermissions = new RepairPermissions
                {
                    CanEditAnyRepair = true,
                    CanCreateRepair = true,
                    CanSeePhone = true,
                    CanSeeSelfPrices = true,
                    CanViewAllRepairs = true,
                },
                ApplicationPermissions = new ApplicationPermissions
                {
                    HasBreakagesAccess = true,
                    HasClientsAccess = true,
                    HasMastersAccess = true,
                    HasReportsAccess = true,
                    HasRolesAccess = true,
                    HasStatusesAccess = true,
                    HasUsersAccess = true,
                },
                Name = "Admin",
                Id = Guid.NewGuid()
            };
            User usr = new User
            {
                Login = Login,
                Password = pass,
                Role = role,
                Id = Guid.NewGuid(),
                Name = "Администратор"
            };

            DbContext.Roles.Add(role);
            DbContext.Users.Add(usr);
            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Error = "Ошбика при создании пользователя";
            }

            ApplicationUser.SetUser(usr);

            DoCloseView();

        }

        public void IsNewUser()
        {
            if (DbContext.Users.Count() == 0)
            {
                IsFirstVisit = true;
            }
        }

        public void DoLogin(object parameter)
        {
            var passwordContainer = parameter as IHavePassword;
            if (passwordContainer != null)
            {
                var secureString = passwordContainer.Password;
                Password = ConvertToUnsecureString(secureString);
            }
            else
            {
                Error = "Пустой пароль!";
                return;
            }

            if (String.IsNullOrEmpty(Login) || String.IsNullOrEmpty(Password))
            {
                Error = "Пустой логин или пароль!";
                return;
            }


            Password = PasswordMaker.CreatMd5(Password);
            var user = DbContext
                .Users
                  .AsNoTracking()
                .Include(x => x.Role)
                  .AsNoTracking()
                .FirstOrDefault(x => x.Login == Login && x.Password == Password);

            if (user != null)
            {
                ApplicationUser.SetUser(user);
                DoCloseView();
            }
            else
            {
                Error = "Такого пользователя нет!";
            }

        }


        // Unsecure password string
        private string ConvertToUnsecureString(System.Security.SecureString securePassword)
        {
            if (securePassword == null)
            {
                return string.Empty;
            }

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return System.Runtime.InteropServices.Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        protected void DoCloseView()
        {

            var handler = OnCloseView;
            if (handler != null)
                OnCloseView();
        }

        ~LoginViewModel()
        {
            DbContext?.Dispose();
        }

    }
}
