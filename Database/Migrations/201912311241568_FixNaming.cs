﻿namespace WorkShop.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FixNaming : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Roles", "CanCrateMaster", "CanCreateMaster");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Roles", "CanCreateMaster", "CanCrateMaster");

        }
    }
}
