﻿using WorkShop.Database;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.IO;
using System.Windows.Media;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Color = System.Drawing.Color;

namespace WorkShop.Views.Menu.Reports
{
    public class ReportViewModel : BaseViewModel
    {
        #region Vars

        private List<Repair> _showedRepairs;

        public event Func<bool> EventCheckoutRepairs;

        private List<string> _reportFields;

        public List<string> ReportFields
        {
            get => _reportFields;
            set
            {
                _reportFields = value;
                OnPropertyChanged("ReportFields");
            }
        }

        public DateTime DateIssuedBegin { get; set; }

        private DateTime _dateIssuedEnd;

        public DateTime DateIssuedEnd
        {
            get => _dateIssuedEnd;
            set => _dateIssuedEnd = value.AddDays(1);
        }

        private List<Master> _mastersFields;

        public List<Master> MastersFields
        {
            get => _mastersFields;
            set
            {
                _mastersFields = value;
                OnPropertyChanged("MastersFields");
            }
        }

        private List<Status> _statusesFields;

        public List<Status> StatusesFields
        {
            get => _statusesFields;
            set
            {
                _statusesFields = value;
                OnPropertyChanged("StatusesFields");
            }
        }

        private List<Client> _clientsFields;

        public List<Client> ClientsFields
        {
            get => _clientsFields;
            set
            {
                _clientsFields = value;
                OnPropertyChanged("ClientsFields");
            }
        }

        private List<User> _acceptersFields;

        public List<User> AcceptersFields
        {
            get => _acceptersFields;
            set
            {
                _acceptersFields = value;
                OnPropertyChanged("AcceptersFields");
            }
        }


        public ObservableCollection<string> ReportListToDisplay { get; set; }

        private ArrayList _reportListValues;
        private RepairsContext _dbContext;

        #endregion



        public ReportViewModel()
        {
            _dbContext = ContextFactory.CreateContext();
            ReportListToDisplay = new ObservableCollection<string>();
            _reportListValues = new ArrayList();

            ReportFields = new List<string>();
            MastersFields = new List<Master>();
            StatusesFields = new List<Status>();
            ClientsFields = new List<Client>();
            AcceptersFields = new List<User>();

            ReportFields.Add("Мастера");
            ReportFields.Add("Статусы");
            ReportFields.Add("Владельцы");
            ReportFields.Add("Принявший");

            try
            {
                MastersFields = _dbContext.Masters.ToList();
                StatusesFields = _dbContext.Statuses.ToList();
                ClientsFields = _dbContext.Clients.ToList();
                AcceptersFields = _dbContext.Repairs
                    .Select(x => x.Accepter)
                    .Distinct()
                    .ToList();
            }
            catch (Exception e)
            {
            }
        }


        public void AddItemToReport(object s)
        {
            try
            {
                if (s is Master)
                {
                    var master = (Master)s;
                    string name = String.Format("Мастер: {0}", master.Name);
                    ReportListToDisplay.Add(name);
                    _reportListValues.Add(master);
                }

                if (s is Status)
                {
                    var status = (Status)s;
                    string name = String.Format("Статус: {0}", status.Name);
                    ReportListToDisplay.Add(name);
                    _reportListValues.Add(status);
                }

                if (s is Client)
                {
                    var client = (Client)s;
                    string name = String.Format("Владелец: {0}", client.Name);
                    ReportListToDisplay.Add(name);
                    _reportListValues.Add(client);
                }

                if (s is User)
                {
                    var accepter = (User)s;
                    string name = String.Format("Принял: {0}", accepter.Name);
                    ReportListToDisplay.Add(name);
                    _reportListValues.Add(accepter);
                }
            }
            catch (Exception e)
            {
            }
        }


        public void RemoveItemFromRepor(int selectedIndex)
        {
            ReportListToDisplay.RemoveAt(selectedIndex);
            _reportListValues.RemoveAt(selectedIndex);
        }


        public void BuildReport()
        {
            _dbContext = ContextFactory.CreateContext();
            bool CheckReport = false;

            IQueryable<Repair> query = _dbContext.Repairs;
            Collection<Guid?> clientsList = new Collection<Guid?>();
            Collection<Guid?> mastersList = new Collection<Guid?>();
            Collection<Guid?> statusesList = new Collection<Guid?>();
            Collection<Guid?> acceptersList = new Collection<Guid?>();

            foreach (object s in _reportListValues)
            {
                if (s is Master)
                {
                    Master master = (Master)s;
                    mastersList.Add(master.Id);
                }

                if (s is Status)
                {
                    Status status = (Status)s;
                    statusesList.Add(status.Id);
                }

                if (s is Client)
                {
                    Client client = (Client)s;
                    clientsList.Add(client.Id);
                }

                if (s is User)
                {
                    User accepter = (User)s;
                    acceptersList.Add(accepter.Id);
                }
            }

            if (clientsList.Count > 0) query = query.Where(x => clientsList.Contains(x.Client.Id));
            if (mastersList.Count > 0) query = query.Where(x => mastersList.Contains(x.Master.Id));
            if (statusesList.Count > 0) query = query.Where(x => statusesList.Contains(x.Status.Id));
            if (acceptersList.Count > 0) query = query.Where(x => acceptersList.Contains(x.Accepter.Id));

            // if date not picked , don't paste date-ranged filter
            if (DateIssuedBegin != DateTime.MinValue)
            {
                CheckReport = true;
                query = query.Where(x => x.DateIsued >= DateIssuedBegin);
            }

            if (DateIssuedEnd != DateTime.MinValue)
            {
                CheckReport = true;
                query = query.Where(x => x.DateIsued <= DateIssuedEnd);
            }


            _showedRepairs = query
                .Include(x => x.Master)
                .Include(x => x.Client)
                .Include(x => x.Accepter)
                .Include(x => x.PriceAgreedBy)
                .OrderByDescending(x => x.CreatedAt)
                .ToList();


            try
            {
                string filePath = Path.Combine(Path.GetTempPath(), $"report_{DateTime.Now.ToString("yyMMdd_hhmmss")}.xlsx");

                using (ExcelPackage excelPackage = new OfficeOpenXml.ExcelPackage())
                {
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Лист1");

                    /*Header*/
                    int i = 1;
                    string headerText = BuildDateRangeHeader();

                    foreach (string item in ReportListToDisplay)
                    {
                        headerText += String.Format("{0} {1}", Environment.NewLine, item);
                        i++;
                    }

                    //ws.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;
                    ExcelRange topHeaderCells = ws.Cells[$"A1:N{i}"];
                    topHeaderCells.Merge = true;
                    topHeaderCells.Style.Font.Bold = true;
                    topHeaderCells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    topHeaderCells.Value = headerText;
                    topHeaderCells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    topHeaderCells.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    i++;

                    ws.Cells[i, 1].Value = "№";
                    ws.Cells[i, 2].Value = "IMEI";
                    ws.Cells[i, 3].Value = "Модель";
                    ws.Cells[i, 4].Value = "Поломка";
                    ws.Cells[i, 5].Value = "Причина";
                    ws.Cells[i, 6].Value = "Владелец";
                    ws.Cells[i, 7].Value = "Мастер";
                    ws.Cells[i, 8].Value = "Приянл";
                    ws.Cells[i, 9].Value = "Чистый доход";
                    ws.Cells[i, 10].Value = "Цена ремонта";
                    ws.Cells[i, 11].Value = "Дата принятия";
                    ws.Cells[i, 12].Value = "Дата выдачи";
                    ws.Cells[i, 13].Value = "Доп. доход";
                    ws.Cells[i, 14].Value = "Менеджер";


                    ExcelRange excelCellsHeader = ws.Cells[$"A{i}:N{i}"];
                    excelCellsHeader.Style.Font.Bold = true;
                    excelCellsHeader.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    excelCellsHeader.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    excelCellsHeader.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    excelCellsHeader.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    i++;

                    /*--End Header--*/

                    /*Content*/
                    int j = 1;
                    int rowContentStarts = i;
                    foreach (var item in _showedRepairs)
                    {
                        ws.Cells[i, 1].Value = j;
                        ws.Cells[i, 2].Value = item.Imei;
                        ws.Cells[i, 3].Value = item.Model;
                        ws.Cells[i, 4].Value = item.Deffect;
                        ws.Cells[i, 5].Value = item.Reason;
                        if (item.Client != null) ws.Cells[i, 6].Value = item.Client.Name;
                        if (item.Master != null) ws.Cells[i, 7].Value = item.Master.Name;
                        if (item.Accepter != null) ws.Cells[i, 8].Value = item.Accepter.Name;
                        ws.Cells[i, 9].Value = item.SelfPrice;
                        ws.Cells[i, 10].Value = item.Price;
                        ws.Cells[i, 11].Value = item.CreatedAt.ToShortDateString();
                        if (CheckReport)
                        {
                            DateTime date = (DateTime)item.DateIsued;
                            ws.Cells[i, 12].Value = date.ToShortDateString();
                        }

                        ws.Cells[i, 13].Value = item.AdditionalIncome;
                        ws.Cells[i, 14].Value = item.PriceAgreedBy?.Name ?? item.Accepter?.Name;

                        if (item.IsPayed)
                        {
                            ws.Cells[$"A{i}:N{i}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[$"A{i}:N{i}"].Style.Fill.BackgroundColor.SetColor(Color.OliveDrab);
                        }


                        i++;
                        j++;
                    }

                    string endRange = String.Format("N{0}", i - 1);
                    ExcelRange excelCellsContent = ws.Cells[$"A{rowContentStarts}:{endRange}"];
                    excelCellsContent.Style.Border.Top.Style = ExcelBorderStyle.Dotted;
                    excelCellsContent.Style.Border.Bottom.Style = ExcelBorderStyle.Dotted;
                    excelCellsContent.Style.Border.Left.Style = ExcelBorderStyle.Dotted;
                    excelCellsContent.Style.Border.Right.Style = ExcelBorderStyle.Dotted;

                    //format for IMEI
                    ExcelRange imeiRange = ws.Cells[$"B{rowContentStarts}:B{i}"];
                    imeiRange.Style.Numberformat.Format = "0";
                    imeiRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    /*--End Content--*/

                    /*Footer*/
                    if (_showedRepairs.Count > 0)
                    {
                        // Сумма для себестоимости
                        ws.Cells[i, 9].Formula = "=Sum(" + ws.Cells[rowContentStarts, 9].Address + ":" + ws.Cells[i - 1, 9].Address + ")";
                        ws.Cells[i, 9].Style.Font.Bold = true;
                        ws.Cells[i, 9].Style.Font.Italic = true;

                        //Сумма для стоимости ремонта
                        ws.Cells[i, 10].Formula = "=Sum(" + ws.Cells[rowContentStarts, 10].Address + ":" + ws.Cells[i - 1, 10].Address + ")";
                        ws.Cells[i, 10].Style.Font.Bold = true;
                        ws.Cells[i, 10].Style.Font.Italic = true;

                        //Сумма для стоимости ремонта
                        ws.Cells[i, 13].Formula = "=Sum(" + ws.Cells[rowContentStarts, 13].Address + ":" + ws.Cells[i - 1, 13].Address + ")";
                        ws.Cells[i, 13].Style.Font.Bold = true;
                        ws.Cells[i, 13].Style.Font.Italic = true;
                    }
                    /*--Footer--*/

                    // fit cell width to text content               
                    ws.Columns.AutoFit();
                    excelPackage.SaveAs(filePath);
                }

                System.Diagnostics.Process.Start(filePath);
                if (CheckReport)
                {
                    if (EventCheckoutRepairs.Invoke())
                    {
                        CheckOut();
                    }
                }
            }
            catch (Exception e)
            {
            }
        }


        private string BuildDateRangeHeader()
        {
            //  if (DateIssuedEnd != EmptyDate) DateIssuedEnd.AddDays(-1);
            var startDateString = DateIssuedBegin == DateTime.MinValue
                ? ""
                : DateIssuedBegin.ToShortDateString();
            var endDateString = DateIssuedEnd == DateTime.MinValue
                ? ""
                : DateIssuedEnd.AddDays(-1).ToShortDateString();

            if (string.IsNullOrEmpty(startDateString) && string.IsNullOrEmpty(endDateString))
            {
                return "Отчёт за весь период";
            }

            var dateRangeBuilder = new StringBuilder("Отчёт ");
            if (!string.IsNullOrEmpty(startDateString))
            {
                dateRangeBuilder.Append($"с {startDateString} ");
            }

            if (!string.IsNullOrEmpty(endDateString))
            {
                dateRangeBuilder.Append($"по {endDateString}");
            }

            return dateRangeBuilder.ToString();
        }


        private void CheckOut()
        {
            foreach (Repair rep in _showedRepairs)
            {
                _dbContext.Repairs.Find(rep.Id).IsPayed = true;
                _dbContext.SaveChanges();
            }
        }
    }
}