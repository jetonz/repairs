﻿using System;
using System.Windows;

namespace WorkShop.Views
{
    /// <summary>
    /// Interaction logic for DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow : Window
    {
        public DialogWindow()
        {
            InitializeComponent();
            this.Val.Text = String.Empty;

            this.OkButton.Click += (s, e) =>
            {
                this.Close();
            };

            this.CancleButton.Click += (s, e) =>
            {
                this.Close();
            };

           
        }
    }
}
