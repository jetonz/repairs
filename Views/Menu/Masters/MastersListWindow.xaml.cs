﻿using WorkShop.Models.Entities;
using System;
using System.Linq;
using System.Windows;

namespace WorkShop.Views.Menu.Masters
{
    /// <summary>
    /// Interaction logic for MastersListView.xaml
    /// </summary>
    public partial class MastersListWindow : Window
    {
        public MastersListWindow()
        {
            InitializeComponent();
            MastersListViewModel VM = new MastersListViewModel();
            this.DataContext = VM;

            this.Edit.Click += (s, e) =>
            {
                if (VM.CurrMaster != null)
                {
                    DialogWindow win = new DialogWindow();
                    win.Val.Text = VM.CurrMaster.Name;
                    win.Title = "Новое имя мастера";
                    win.ShowDialog();

                    while (win.IsVisible)
                    {

                    }
                    VM.CurrMaster.Name = win.Val.Text;
                    VM.MastersList.Where(x => x.Id == VM.CurrMaster.Id).FirstOrDefault().Name = win.Val.Text;
                    VM.EditMaster();
                }
            };

            this.Create.Click += (s, e) =>
            {
                DialogWindow win = new DialogWindow();
                win.Title = "Создать мастера";
                win.ShowDialog();
                while (win.IsVisible)
                {

                }
                if (!String.IsNullOrEmpty(win.Val.Text))
                {
                    VM.CreateMaster(win.Val.Text);
                }
            };

            this.Delete.Click += (s, e) =>
            {
                if (VM.CurrMaster != null)
                {
                    VM.DeleteMaster(VM.CurrMaster);
                }
            };

            this.MasterList.SelectionChanged += (s, e) =>
            {
                VM.CurrMaster = (Master)this.MasterList.SelectedItem;
            };


        }
    }
}
