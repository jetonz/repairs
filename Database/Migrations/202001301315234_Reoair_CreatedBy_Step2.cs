﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reoair_CreatedBy_Step2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Repair", new[] { "CreatedBy_Id" });
            AlterColumn("dbo.Repair", "CreatedBy_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.Repair", "CreatedBy_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Repair", new[] { "CreatedBy_Id" });
            AlterColumn("dbo.Repair", "CreatedBy_Id", c => c.Guid());
            CreateIndex("dbo.Repair", "CreatedBy_Id");
        }
    }
}
