﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditHistoryAction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RepairEditHistory", "Action", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RepairEditHistory", "Action");
        }
    }
}
