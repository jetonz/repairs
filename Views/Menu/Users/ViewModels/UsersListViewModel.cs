﻿using WorkShop.Database;
using WorkShop.Libs;
using WorkShop.Models;
using WorkShop.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace WorkShop.Views.Menu.Users
{
    class UsersListViewModel : BaseViewModel
    {
        RepairsContext DbContext;

        public UserInfo SelectedUser { get; set; }
        public ObservableCollection<UserInfo> _userlist;
        public ObservableCollection<UserInfo> Userlist
        {
            get
            {
                return _userlist;
            }
            set
            {
                _userlist = value;
                OnPropertyChanged("Userlist");
            }
        }

        public ICommand Delete { get; private set; }

        public UsersListViewModel()
        {         
            Delete = new RelayCommand(DeleteUser);

            DbContext = ContextFactory.CreateContext();
            Userlist = new ObservableCollection<UserInfo>();
            var UsrInDB = DbContext.Users.ToList();     
            foreach(var usr in UsrInDB)
            {
                Userlist.Add(new UserInfo(usr));
            }
        }

        public void EditUser()
        {
            DbContext = ContextFactory.CreateContext();

            Userlist.Clear();
            try
            {
                var UsrInDB = DbContext.Users.ToList();
                foreach (var usr in UsrInDB)
                {
                    Userlist.Add(new UserInfo(usr));
                }
            }
            catch (Exception e)
            {

            }
        }

        public void CreateUser()
        {
            Userlist.Clear();
            try
            {
                var UsrInDB = DbContext.Users.ToList();
                foreach (var usr in UsrInDB)
                {
                    Userlist.Add(new UserInfo(usr));
                }
            }
            catch(Exception e)
            {

            }            
        }

        private void DeleteUser(object param)
        {
            if(SelectedUser != null)
            {
                var user = DbContext.Users.Find(SelectedUser.Id);
                DbContext.Users.Remove(user);
                if( DbContext.SaveChanges() == 1)
                {
                    Userlist.Remove(SelectedUser);
                }

            }
            
        }
    }
}

