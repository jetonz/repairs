﻿using System.ComponentModel;

namespace WorkShop.Models
{

    //Addding description to enum properties will poulate it at combobox!!!

    public enum ERepairUrgency : short
    {
        [Description("По умолчанию")]
        DefaulRepair,
               
        StreetRepair,

        [Description("Срочный ремонт")]
        UrgentRepair,

        [Description("Клиентский ремонт")]
        Clientrepair
    }
}
