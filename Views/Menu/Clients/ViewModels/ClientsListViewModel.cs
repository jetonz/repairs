﻿using WorkShop.Database;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace WorkShop.Views.Menu.Clients
{
    class ClientsListViewModel : BaseViewModel
    {

        private ObservableCollection<Client> _clientsList;
        public ObservableCollection<Client> ClientsList
        {
            get
            {
                return _clientsList;
            }
            set
            {
                _clientsList = value;
                OnPropertyChanged("ClientsList");
            }
        }

        RepairsContext DbConnection;
        public Client CurrentClient { get; set; }

        public ClientsListViewModel()
        {
            DbConnection = ContextFactory.CreateContext();
            ClientsList = new ObservableCollection<Client>(DbConnection.Clients.ToList());
        }

        public void EditClient()
        {
            DbConnection.Entry(CurrentClient).State = System.Data.Entity.EntityState.Modified;
            DbConnection.SaveChanges();
        }

        public void CreateClient(string clientName)
        {
            Client client = new Client() { Id = Guid.NewGuid(), Name = clientName };
            DbConnection.Clients.Add(client);
            DbConnection.SaveChanges();
            ClientsList.Add(client);
        }

        public void DeleteClient(Client client)
        {
            DbConnection.Clients.Remove(client);
            if (DbConnection.SaveChanges() == 1)
            {
                ClientsList.Remove(client);
                CurrentClient = null;
            }
        }
    }
}
