﻿using System.Windows;
using System.Text.RegularExpressions;
using System;

namespace WorkShop.Views.Menu.Breakages
{
    /// <summary>
    /// Interaction logic for BreaekageDetailsView.xaml
    /// </summary>
    public partial class BreakageDetailsWindow : Window
    {

        public BreakageDetailsWindow(BreakageDetailsViewModel breakage) : base()
        {
            InitializeComponent();
            this.DataContext = breakage;

            this.InternalPrice.PreviewTextInput += (s, e) =>
            {
                e.Handled = !e.Text.IsNumber();
            };

            this.ExternalPrice.PreviewTextInput += (s, e) =>
            {
                e.Handled = !e.Text.IsNumber();
            };

            this.BtnCancel.Click += (s, e) =>
            {
                this.Close();
            };

            this.BtnSave.Click += (s, e) =>
            {
                breakage.SaveChanges();
                this.Close();
            };
        }



    }
}
