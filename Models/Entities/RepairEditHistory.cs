﻿using System;

namespace WorkShop.Models.Entities
{
    public class RepairEditHistory : BaseEntity<int>
    {
        public int RepairId { get; set; }
        public Guid? StatusId { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public ERepairHistoryAction Action { get; set; }


        public virtual Repair Repair { get; set; }
        public virtual Status Status { get; set; }
        public virtual User User { get; set; }
    }
}
