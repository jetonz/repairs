﻿using WorkShop.Database;
using WorkShop.Libs;
using WorkShop.Models;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace WorkShop.Views.Menu.Users
{
    public class UserDetailsVeiwModel : BaseViewModel
    {
        RepairsContext DbContext;

        private string OldPass;
        public UserInfo user { get; set; }

        private Role _CurrRole;
        public Role CurrRole
        {
            get
            {
                return _CurrRole;
            }
            set
            {
                _CurrRole = value;
                OnPropertyChanged("CurrRole");
            }
        }

        public List<Role> AllRoles { get; set; }

        public Action OnCloseView;


        private string _Error;
        public string Error
        {
            get
            {
                return _Error;
            }
            set
            {
                _Error = value;
                OnPropertyChanged("Error");
            }
        }


        public ICommand Action { get; private set; }

        public UserDetailsVeiwModel()
        {
            user = new UserInfo();
            GetValuesForBox();
        }

        public UserDetailsVeiwModel(UserInfo usr)
        {
            user = usr;
            OldPass = usr.Password;
            GetValuesForBox();
        }

        private void GetValuesForBox()
        {
            DbContext = ContextFactory.CreateContext();
            AllRoles = DbContext.Roles.ToList();

            if (Guid.Empty == user.Id)
            {
                Action = new RelayCommand(CreateUser);
            }
            else
            {
                Action = new RelayCommand(EditUser);
            }

            try
            {
                CurrRole = DbContext.Roles.Where(x => x.Id == user.CurrRole.Id).FirstOrDefault();
            }
            catch (Exception e)
            {
                // fuck it
            }
        }


        private void EditUser(object obj)
        {
            if (String.IsNullOrEmpty(user.Login) || String.IsNullOrEmpty(user.Password) || String.IsNullOrEmpty(user.Name))
            {
                Error = "Не все поля заполнены";
                return;
            }
            if (CurrRole.Id == null)
            {
                Error = "Выберите роль!";
                return;
            }
            try
            {
                if (!OldPass.Equals(user.Password))
                {
                    user.Password = PasswordMaker.CreatMd5(user.Password);
                }
                User DbUser = DbContext.Users.Where(x => x.Id == user.Id).FirstOrDefault();
                DbUser.Login = user.Login;
                DbUser.Password = user.Password;
                DbUser.Name = user.Name;
                DbUser.Role = CurrRole;

                DbContext.Entry(DbUser).State = System.Data.Entity.EntityState.Modified;
                DbContext.SaveChanges();
                DoCloseView();
            }
            catch (Exception e)
            {
                Error = e.Message;
            }
        }

        private void CreateUser(object obj)
        {
            if (String.IsNullOrEmpty(user.Login) || String.IsNullOrEmpty(user.Password) || String.IsNullOrEmpty(user.Name))
            {
                Error = "Не все поля заполнены";
                return;
            }
            if (CurrRole == null)
            {
                Error = "Выберите роль!";
                return;
            }

            try
            {
                User usr = new User()
                {
                    Id = Guid.NewGuid(),
                    Login = user.Login,
                    Name = user.Name,
                    Password = PasswordMaker.CreatMd5(user.Password),
                    Role = CurrRole
                };
                DbContext.Users.Add(usr);
                DbContext.SaveChanges();
                DoCloseView();
            }
            catch (Exception e)
            {
                Error = e.Message;
            }

        }

        protected void DoCloseView()
        {

            var handler = OnCloseView;
            if (handler != null)
                OnCloseView();
        }

    }
}
