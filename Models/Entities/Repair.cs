﻿using System;
using System.Collections.Generic;

namespace WorkShop.Models.Entities
{
    public class Repair : BaseEntity<int>
    {
        private Repair()
        {
            this.RepairMastersHistory = new HashSet<RepairMasterHistory>();
            this.RepairEditHistory = new HashSet<RepairEditHistory>();
        }

        public Repair(User createdBy) : this()
        {
            CreatedBy = createdBy;
        }

        public string Imei { get; set; }
        public string Model { get; set; }
        public string Deffect { get; set; }
        public string Reason { get; set; }
        public Guid LastEdited { get; set; }
        public DateTime CreatedAt { get; private set; } = DateTime.UtcNow;
        public DateTime? DateIsued { get; private set; }
        public decimal Price { get; set; }
        public decimal WorkPrice { get; set; }
        public decimal SuppliesPrice { get; set; }
        public decimal SelfPrice
        {
            get
            {
                var res = Price - SuppliesPrice;
                return res > WorkPrice
                    ? WorkPrice
                    : res;
            }
        }
        public decimal AdditionalIncome
        {
            get
            {
                var addIncome = Price - WorkPrice - SuppliesPrice;
                if (addIncome < 0)
                {
                    return 0;
                }
                else
                {
                    return addIncome;
                }
            }
        }

        public decimal Expenses => WorkPrice + SuppliesPrice;

        public string Comment { get; set; }
        public string InvoiceNumber { get; set; }
        public bool IsPayed { get; set; }
        public string Phone { get; set; }
        public ERepairUrgency Urgency { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string FIO { get; set; }
        public string PhoneDesc { get; set; }
         

        public virtual Master Master { get; set; }
 
        public virtual Client Client { get; private set; }
 
        public virtual User Accepter { get; set; }

        private Status _status; 
        public virtual Status Status
        {
            get => _status;
            set
            {
                if(value != null && value.IsIssueStatus)
                {
                    DateIsued = DateTime.UtcNow;
                }
                else if(value != null && !value.IsIssueStatus)
                {
                    DateIsued = null;
                }
                _status = value;
            }
        }
         
        public virtual Breakage Breakage { get; set; }
         

        private User _priceAgreedBy;
        public User PriceAgreedBy
        {
            get => _priceAgreedBy;
            set
            {
                _priceAgreedBy = value;
                PriceAgreedAt = DateTime.UtcNow;
            }
        }

        public User CreatedBy { get; private set; }

        public DateTime? PriceAgreedAt { get; private set; }

        public virtual ICollection<RepairMasterHistory> RepairMastersHistory { get; set; }

        public virtual ICollection<RepairEditHistory> RepairEditHistory { get; set; }

        public void SetClient(Client client)
        { 
            Client = client;
            RefreshUrgency();
        }

        public void SetClient(Guid clientId)
        { 
            RefreshUrgency();
        }

        private void RefreshUrgency()
        {
            if (Client != null && Client.Id == Constants.DefaultExternalClient && Urgency == ERepairUrgency.DefaulRepair)
            {
                Urgency = ERepairUrgency.StreetRepair;
            }
        }
    }
}