﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WorkShop.Libs
{
    static class PasswordMaker
    {

        public static string CreatMd5(string Str)
        {
            byte[] encodedStr = new UTF8Encoding().GetBytes(Str);

            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedStr);

            string encoded = BitConverter.ToString(hash)
               .Replace("-", string.Empty)
               .ToLower();

            return encoded;
        }
    }
}
