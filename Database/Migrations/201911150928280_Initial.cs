namespace WorkShop.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Masters",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RepairMastersHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RepairId = c.Int(nullable: false),
                        MasterId = c.Guid(),
                        DateChanged = c.DateTime(nullable: false),
                        StatusId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Masters", t => t.MasterId)
                .ForeignKey("dbo.Statuses", t => t.StatusId)
                .ForeignKey("dbo.Repair", t => t.RepairId, cascadeDelete: true)
                .Index(t => t.RepairId)
                .Index(t => t.MasterId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.Repair",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Imei = c.String(),
                        Model = c.String(),
                        Deffect = c.String(),
                        Reason = c.String(),
                        LastEdited = c.Guid(nullable: false),
                        DateApply = c.DateTime(nullable: false),
                        DateIsued = c.DateTime(),
                        RepairPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SelfPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WorkPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SuppliesPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comment = c.String(),
                        Realization = c.String(),
                        IsPayed = c.Boolean(nullable: false),
                        Phone = c.String(),
                        Urgency = c.Short(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        FIO = c.String(),
                        PhoneDesc = c.String(),
                        MasterId = c.Guid(),
                        OwnerId = c.Guid(nullable: false),
                        AccepterId = c.Guid(),
                        StatusId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Statuses", t => t.StatusId)
                .ForeignKey("dbo.Users", t => t.AccepterId)
                .ForeignKey("dbo.Masters", t => t.MasterId)
                .ForeignKey("dbo.Owners", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.MasterId)
                .Index(t => t.OwnerId)
                .Index(t => t.AccepterId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        RoleId = c.Guid(nullable: false),
                        Password = c.String(),
                        Login = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.RepairEditHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RepairId = c.Int(nullable: false),
                        StatusId = c.Guid(),
                        UserId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Repair", t => t.RepairId, cascadeDelete: true)
                .ForeignKey("dbo.Statuses", t => t.StatusId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RepairId)
                .Index(t => t.StatusId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Statuses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StatusName = c.String(),
                        IsIssueStatus = c.Boolean(nullable: false),
                        UseFormula = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CanEditPrice = c.Boolean(nullable: false),
                        CanCreateUser = c.Boolean(nullable: false),
                        CanCrateMaster = c.Boolean(nullable: false),
                        CanCreateReport = c.Boolean(nullable: false),
                        CanCreateStatus = c.Boolean(nullable: false),
                        CanCreateroles = c.Boolean(nullable: false),
                        CanCreateOwner = c.Boolean(nullable: false),
                        CanCreateRepair = c.Boolean(nullable: false),
                        CanSeeSelfPrice = c.Boolean(nullable: false),
                        CanSeePhone = c.Boolean(nullable: false),
                        CanEditMaster = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Owners",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RepairMastersHistory", "RepairId", "dbo.Repair");
            DropForeignKey("dbo.Repair", "OwnerId", "dbo.Owners");
            DropForeignKey("dbo.Repair", "MasterId", "dbo.Masters");
            DropForeignKey("dbo.Repair", "AccepterId", "dbo.Users");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RepairEditHistory", "UserId", "dbo.Users");
            DropForeignKey("dbo.Repair", "StatusId", "dbo.Statuses");
            DropForeignKey("dbo.RepairMastersHistory", "StatusId", "dbo.Statuses");
            DropForeignKey("dbo.RepairEditHistory", "StatusId", "dbo.Statuses");
            DropForeignKey("dbo.RepairEditHistory", "RepairId", "dbo.Repair");
            DropForeignKey("dbo.RepairMastersHistory", "MasterId", "dbo.Masters");
            DropIndex("dbo.RepairEditHistory", new[] { "UserId" });
            DropIndex("dbo.RepairEditHistory", new[] { "StatusId" });
            DropIndex("dbo.RepairEditHistory", new[] { "RepairId" });
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Repair", new[] { "StatusId" });
            DropIndex("dbo.Repair", new[] { "AccepterId" });
            DropIndex("dbo.Repair", new[] { "OwnerId" });
            DropIndex("dbo.Repair", new[] { "MasterId" });
            DropIndex("dbo.RepairMastersHistory", new[] { "StatusId" });
            DropIndex("dbo.RepairMastersHistory", new[] { "MasterId" });
            DropIndex("dbo.RepairMastersHistory", new[] { "RepairId" });
            DropTable("dbo.Owners");
            DropTable("dbo.Roles");
            DropTable("dbo.Statuses");
            DropTable("dbo.RepairEditHistory");
            DropTable("dbo.Users");
            DropTable("dbo.Repair");
            DropTable("dbo.RepairMastersHistory");
            DropTable("dbo.Masters");
        }
    }
}
