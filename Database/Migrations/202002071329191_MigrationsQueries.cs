﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationsQueries : DbMigration
    {
        public override void Up()
        {
            //Old history items set up as update status
            Sql(@"
UPDATE [dbo].[RepairEditHistory]
   SET [Action] = 1
GO
");

            Sql($@"
UPDATE [dbo].[Clients]
   SET [IsExternal] = 1
 WHERE Id = '{Constants.DefaultExternalClient}'
GO
");
        }
        
        public override void Down()
        {
        }
    }
}
