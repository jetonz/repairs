﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reoair_CreatedBy_Step1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Repair", "CreatedBy_Id", c => c.Guid());
            CreateIndex("dbo.Repair", "CreatedBy_Id");
            AddForeignKey("dbo.Repair", "CreatedBy_Id", "dbo.Users", "Id");

            Sql(@" 
                 UPDATE[dbo].[Repair]
                SET [CreatedBy_Id] =[Accepter_Id]");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Repair", "CreatedBy_Id", "dbo.Users");
            DropIndex("dbo.Repair", new[] { "CreatedBy_Id" });
            DropColumn("dbo.Repair", "CreatedBy_Id");
        }
    }
}
