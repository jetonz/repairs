﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CanAgreePrices : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Roles", "RepairPermissions_CanAgreePrice", c => c.Boolean(nullable: false));
            DropColumn("dbo.Roles", "RepairPermissions_CanSeeClient");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Roles", "RepairPermissions_CanSeeClient", c => c.Boolean(nullable: false));
            DropColumn("dbo.Roles", "RepairPermissions_CanAgreePrice");
        }
    }
}
