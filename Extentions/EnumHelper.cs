﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WorkShop.Extentions
{
    public static class EnumHelper
    {
        public static string Description(this Enum eValue)
        {
            var nAttributes = eValue.GetType().GetField(eValue.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (nAttributes.Any())
                return (nAttributes.First() as DescriptionAttribute).Description;

            return string.Empty;
            // If no description is found, the least we can do is replace underscores with spaces
            // You can add your own custom default formatting logic here
            //TextInfo oTI = CultureInfo.CurrentCulture.TextInfo;
            //return oTI.ToTitleCase(oTI.ToLower(eValue.ToString().Replace("_", " ")));
        }

        public static IEnumerable<KeyValuePair<object, object>> GetAllValuesAndDescriptions(Type t)
        {
            if (!t.IsEnum)
                throw new ArgumentException("t must be an enum type");

            var valuesWithDescr = Enum.GetValues(t).Cast<Enum>()
                             .Where(x => !string.IsNullOrEmpty(x.Description()));

            return valuesWithDescr
                    .Select((e) => new KeyValuePair<object, object>(e, e.Description() ))                
                    .ToList();
        }
    }
}
