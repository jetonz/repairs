﻿using WorkShop.Database;
using WorkShop.Models;
using WorkShop.Models.Entities;
using WorkShop.ViewModels;
using System; 
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using WorkShop.Libs;
using System.Windows.Forms;

namespace WorkShop.Views.Repairs
{
    public enum NavigationOperation
    {
        NEXT = 1,
        PREV = 2,
        REFRESH = 3,
        //   SHOW_WARN = 4
    }

    internal class RepairsListViewModel : BaseViewModel
    {
        private const int PAGE_SIZE = 50;
        private readonly User _currentAppUser;
        private int _currentPage = 1;
        private Timer _searchTimer;
        private EventHandler _searchTimerEventHandler;

        private readonly Master EmptyMasterFilterVal = new Master { Name = "Все" };
        private readonly User EmptyUserFilterVal = new User { Name = "Все" };
        private readonly Status EmptyStatusFilterVal = new Status { Name = "Любой" };


        public bool CanEditUser => _currentAppUser.Role.ApplicationPermissions.HasUsersAccess;
        public bool CanEditMaster => _currentAppUser.Role.ApplicationPermissions.HasMastersAccess;
        public bool CanEditReports => _currentAppUser.Role.ApplicationPermissions.HasReportsAccess;
        public bool CanEditStatus => _currentAppUser.Role.ApplicationPermissions.HasStatusesAccess;
        public bool CanEditRoles => _currentAppUser.Role.ApplicationPermissions.HasRolesAccess;
        public bool CanEditBreakage => _currentAppUser.Role.ApplicationPermissions.HasBreakagesAccess;
        public bool CanEditClients => _currentAppUser.Role.ApplicationPermissions.HasClientsAccess;
        public bool CanCreateRepair => _currentAppUser.Role.RepairPermissions.CanCreateRepair;
        public bool CanSeeSelfPrice => _currentAppUser.Role.RepairPermissions.CanSeeSelfPrices;
        public bool CanSeeAllRepairs => _currentAppUser.Role.RepairPermissions.CanViewAllRepairs;
        public string CueerntUserName => _currentAppUser.Name;

        public bool HasFilters { get; private set; }


        #region Vars
        public ObservableCollection<RepairInfo> RepairList { get; set; }

        public ObservableCollection<Master> _mastersFilterList;
        public ObservableCollection<Master> MastersFilterList
        {
            get
            {
                return _mastersFilterList;
            }
            set
            {
                _mastersFilterList = value;
                OnPropertyChanged("MastersFilterList");
            }
        }

        public ObservableCollection<Status> _statusesFilterList;
        public ObservableCollection<Status> StatusesFilterList
        {
            get
            {
                return _statusesFilterList;
            }
            set
            {
                _statusesFilterList = value;
                OnPropertyChanged("StatusesFilterList");
            }
        }

        public ObservableCollection<User> _acceptersFilterList;
        public ObservableCollection<User> AcceptersFilterList
        {
            get
            {
                return _acceptersFilterList;
            }
            set
            {
                _acceptersFilterList = value;
                OnPropertyChanged("AcceptersFilterList");
            }
        }


        private bool _IsWarningFiltered;
        public bool IsWarningFiltered
        {
            get
            {
                return _IsWarningFiltered;
            }
            set
            {
                _IsWarningFiltered = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH);
                OnPropertyChanged("IsWarningFiltered");
            }
        }


        private string _IMEIFilter = String.Empty;
        public string IMEIFilter
        {
            get
            {
                return _IMEIFilter;
            }
            set
            {
                _IMEIFilter = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH);
                OnPropertyChanged("IMEIFilter");
            }
        }

        private string _modelFilter = String.Empty;
        public string ModelFilter
        {
            get
            {
                return _modelFilter;
            }
            set
            {
                _modelFilter = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH);
                OnPropertyChanged("ModelFilter");
            }
        }

        private string _clientsFilter;
        public string ClientsFilter
        {
            get
            {
                return _clientsFilter;
            }
            set
            {
                _clientsFilter = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH);
                OnPropertyChanged("ClientsFilter");
            }
        }

        private string _deffectFilter;
        public string DeffectFilter
        {
            get
            {
                return _deffectFilter;
            }
            set
            {
                _deffectFilter = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH);
                OnPropertyChanged("DeffectFilter");
            }
        }

        private Master _masterFilter;
        public Master MasterFilter
        {
            get
            {
                return _masterFilter;
            }
            set
            {
                _masterFilter = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH, delay: 100);
                OnPropertyChanged("MasterFilter");
            }
        }

        private Status _statusFilter;
        public Status StatusFilter
        {
            get
            {
                return _statusFilter;
            }
            set
            {
                _statusFilter = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH, delay: 100);
                OnPropertyChanged("StatusFilter");
            }
        }

        private User _accepterFilter;
        public User AccepterFilter
        {
            get
            {
                return _accepterFilter;
            }
            set
            {
                _accepterFilter = value;
                UpdateHasFilters();
                LoadRepairList(NavigationOperation.REFRESH, delay: 100);
                OnPropertyChanged("AccepterFilter");
            }
        }


        private int _maxRepairsNum;
        public int MaxRepairsNum
        {
            get
            {
                return _maxRepairsNum;
            }
            set
            {
                _maxRepairsNum = value;
                OnPropertyChanged("MaxRepairsNum");
            }
        }


        private int _warningNum;
        public int WarningNum
        {
            get
            {
                return _warningNum;
            }
            set
            {
                _warningNum = value;
                OnPropertyChanged("WarningNum");
            }
        }

        //   private int _CurrRepairsNum;
        public int CurrRepairsNum => RepairList.Count + (_currentPage * PAGE_SIZE - PAGE_SIZE);


        public ICommand EditRepair { get; set; }
        public ICommand GridListOperation { get; set; }

        public Action<RepairDetailsViewModel> OpenWindowHandler;

        #endregion

        public RepairsListViewModel()
        {
            _currentAppUser = ApplicationUser.GetInstance().DbUser;

            EditRepair = new RelayCommand(OpenEditWindow);
            RepairList = new ObservableCollection<RepairInfo>();

            MastersFilterList = new ObservableCollection<Master>();
            StatusesFilterList = new ObservableCollection<Status>();
            AcceptersFilterList = new ObservableCollection<User>();

            LoadFilters();
            ResetFilters(); // set default empty values on window opening
            LoadRepairList(NavigationOperation.REFRESH);
        }

        public void LoadRepairList(NavigationOperation op, int delay = 500)
        {
            

            if (_searchTimer == null)
            {
                _searchTimer = new Timer { Enabled = true, Interval = delay };
                _searchTimer.Tick += GetTimerHanlder(op);
            }
            else
            {
                _searchTimer.Stop();
                _searchTimer.Start();
            }
        }

        private void RevokeQueryTimer()
        {
            if (_searchTimer != null)
            {                
                _searchTimer.Stop();
                _searchTimer.Tick -= GetTimerHanlder(NavigationOperation.REFRESH);
                _searchTimer = null;
            }
        }       

        private EventHandler GetTimerHanlder(NavigationOperation operation)
        {
            _searchTimerEventHandler = (s, e) =>
            {
                RevokeQueryTimer();
                DoLoadRepairList(operation);
            };

            return _searchTimerEventHandler;
        }

 

        private void DoLoadRepairList(NavigationOperation op)
        { 
            using (var context = ContextFactory.CreateContext())
            {
                WarningNum = QueryRepairsWithWarnings(context.Repairs).Count();

                IQueryable<Repair> query = context.Repairs.AsNoTracking();

                if (IsWarningFiltered)
                {
                    query = QueryRepairsWithWarnings(query);
                }

                query = QueryRepairsWithFilters(query);


                switch (op)
                {
                    case (NavigationOperation.REFRESH):
                        _currentPage = 1;
                        MaxRepairsNum = query.Count();
                        break;
                    case (NavigationOperation.NEXT):
                        if (MaxRepairsNum > _currentPage * PAGE_SIZE)
                        {
                            _currentPage++;
                        }
                        else
                        {
                            return;
                        }
                        break;
                    case (NavigationOperation.PREV):
                        if (_currentPage * PAGE_SIZE > PAGE_SIZE)
                        {
                            _currentPage--;
                        }
                        else
                        {
                            return;
                        }
                        break;
                }

                RepairList.Clear();

                query
                   .OrderByDescending(x => x.Id)
                   .Skip(PAGE_SIZE * _currentPage - PAGE_SIZE)
                   .Take(PAGE_SIZE)
                   .SelectRepairInfo()
                   .ToList()
                   .ForEach(i => RepairList.Add(i));

                OnPropertyChanged("CurrRepairsNum");
            }

        }

        public void LoadFilters()
        {
            using (var context = ContextFactory.CreateContext())
            {
                StatusesFilterList.Clear();
                MastersFilterList.Clear();
                AcceptersFilterList.Clear();

                var masters = context.Repairs
                    .AsNoTracking()
                    .Select(x => x.Master)
                    .OrderBy(x => x.Name)
                    .Distinct()
                    .ToList();

                var statuses = context.Repairs
                    .AsNoTracking()
                    .Select(x => x.Status)
                    .OrderBy(x => x.Name)
                    .Distinct()
                    .ToList();

                var accepters = context.Repairs
                    .AsNoTracking()
                    .Select(x => x.Accepter)
                    .OrderBy(x => x.Name)
                    .Distinct()
                    .ToList();

                foreach (var item in masters)
                {
                    MastersFilterList.Add(item);
                }
                foreach (var item in statuses)
                {
                    StatusesFilterList.Add(item);
                }
                foreach (var item in accepters)
                {
                    AcceptersFilterList.Add(item);
                }

                StatusesFilterList.Insert(0, EmptyStatusFilterVal);
                MastersFilterList.Insert(0, EmptyMasterFilterVal);
                AcceptersFilterList.Insert(0, EmptyUserFilterVal);
            }
        }

        public void ResetFilters()
        {
            MasterFilter = EmptyMasterFilterVal;
            StatusFilter = EmptyStatusFilterVal;
            AccepterFilter = EmptyUserFilterVal;
            ClientsFilter = String.Empty;
            IMEIFilter = String.Empty;
            ModelFilter = String.Empty;
            DeffectFilter = String.Empty;
            IsWarningFiltered = false;
        }

        public void OpenEditWindow(object sender)
        {
            try
            {
                if (OpenWindowHandler != null)
                {
                    if (sender is RepairInfo rep)
                    {
                        RepairDetailsViewModel repVM = new RepairDetailsViewModel(rep.Id);
                        OpenWindowHandler(repVM);
                    }
                    else
                    {
                        RepairDetailsViewModel repVM = new RepairDetailsViewModel();
                        OpenWindowHandler(repVM);
                    }
                }
            }
            catch (Exception e)
            {
                if (OpenWindowHandler != null)
                {
                    RepairDetailsViewModel repVM = new RepairDetailsViewModel();
                    OpenWindowHandler(repVM);
                }
            }
        }


        private void UpdateHasFilters()
        {
            if (!string.IsNullOrEmpty(ModelFilter))
            {
                this.HasFilters = true;
            }
            else if (!string.IsNullOrEmpty(ClientsFilter))
            {
                this.HasFilters = true;
            }
            else if (!string.IsNullOrEmpty(DeffectFilter))
            {
                this.HasFilters = true;
            }
            else if (!string.IsNullOrEmpty(IMEIFilter))
            {
                this.HasFilters = true;
            }
            else if (MasterFilter?.Name != EmptyMasterFilterVal.Name)
            {
                this.HasFilters = true;
            }
            else if (StatusFilter?.Name != EmptyStatusFilterVal.Name)
            {
                this.HasFilters = true;
            }
            else if (AccepterFilter?.Name != EmptyUserFilterVal.Name)
            {
                this.HasFilters = true;
            }
            else if (IsWarningFiltered)
            {
                this.HasFilters = true;
            }
            else
            {
                this.HasFilters = false;
            }

            OnPropertyChanged(nameof(HasFilters));
        }

        private IQueryable<Repair> QueryRepairsWithWarnings(IQueryable<Repair> query)
        {
            var clientOffsetDate = DateTime.UtcNow.AddDays(-3);
            var urgentOffsetDate = DateTime.UtcNow.AddDays(-1);
            var streeOffsetDate = DateTime.UtcNow.AddDays(-5);

            query = query
                .Where(x => (x.Urgency == ERepairUrgency.Clientrepair && x.UpdatedAt < clientOffsetDate
                          || x.Urgency == ERepairUrgency.UrgentRepair && x.UpdatedAt < urgentOffsetDate
                          || x.Urgency == ERepairUrgency.StreetRepair && x.UpdatedAt < streeOffsetDate)
                         && x.Status.Id == Constants.InRepairStatusId);

            return query;
        }

        private IQueryable<Repair> QueryRepairsWithFilters(IQueryable<Repair> query)
        {
            query = (IsWarningFiltered)
                                     ? QueryRepairsWithWarnings(query)
                                     : query;

            query = query.Where(x => x.Imei.Contains(IMEIFilter)
                    && x.Model.Contains(ModelFilter));

            if (StatusFilter != null && StatusFilter.Id != Guid.Empty)
            {
                query = query.Where(x => x.Status.Id == StatusFilter.Id);
            }

            if (!String.IsNullOrEmpty(ClientsFilter))
            {
                query = query.Where(x => x.Client.Name.Contains(ClientsFilter));
            }

            if (MasterFilter != null && MasterFilter.Id != Guid.Empty)
            {
                query = query.Where(x => x.Master.Id == MasterFilter.Id);
            }

            if (AccepterFilter != null && AccepterFilter.Id != Guid.Empty)
            {
                query = query.Where(x => x.Accepter.Id == AccepterFilter.Id);
            }

            if (!String.IsNullOrEmpty(DeffectFilter))
            {
                query = query.Where(x => x.Deffect.Contains(DeffectFilter));
            }

            if (!CanSeeAllRepairs)
            {
                query = query.Where(x => x.CreatedBy.Id == _currentAppUser.Id);
            }

            return query;
        }
    }
}
