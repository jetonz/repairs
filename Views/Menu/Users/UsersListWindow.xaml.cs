﻿using WorkShop.Models;
using System.Windows;

namespace WorkShop.Views.Menu.Users
{
    /// <summary>
    /// Interaction logic for UserListWindow.xaml
    /// </summary>
    public partial class UsersListWindow : Window
    {
        public UsersListWindow()
        {
            InitializeComponent();
            UsersListViewModel vm = new UsersListViewModel();
            this.DataContext = vm;

            this.dataGrid.SelectionChanged += (s, e) =>
            {
                vm.SelectedUser = (UserInfo)this.dataGrid.SelectedItem;
            };

            this.create.Click += (s, e) =>
            {
                UserDetailsWindow win = new UserDetailsWindow();
                win.Title = "Создать пользователя";
                win.ShowDialog();
                while(win.IsVisible) {
                    
                }
                vm.CreateUser();
            };

            this.edit.Click += (s, e) =>
             {
                 if(vm.SelectedUser != null)
                 {
                     UserDetailsWindow win = new UserDetailsWindow(vm.SelectedUser);
                     win.Title = "Редактировать пользователя";
                     win.ShowDialog();
                     while (win.IsVisible)
                     {

                     }
                     vm.EditUser();
                 }
                
             };

        }
    }
}
