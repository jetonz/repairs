﻿using System;

namespace WorkShop.Models.Entities
{
    public partial class RepairMasterHistory : BaseEntity<int>
    {
        public int RepairId { get; set; }
        public Guid? MasterId { get; set; }
        public DateTime DateChanged { get; set; }
        public Guid? StatusId { get; set; } 

        public virtual Master Master { get; set; }
        public virtual Repair Repair { get; set; }
        public virtual Status Status { get; set; }
    }
}
