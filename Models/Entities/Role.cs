﻿using System;
using System.Collections.Generic;

namespace WorkShop.Models.Entities
{
    public class Role : BaseEntity<Guid>
    {
        public Role()
        {
            this.Users = new HashSet<User>();
        }

        public string Name { get; set; }

        public ApplicationPermissions ApplicationPermissions { get; set; }
        public RepairPermissions RepairPermissions { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }

    public class RepairPermissions
    {
        public bool CanViewAllRepairs { get; set; }
        public bool CanCreateRepair { get; set; }
        public bool CanEditAnyRepair { get; set; }

        public bool CanSeePhone { get; set; }
        public bool CanAddClients { get; set; }
        public bool CanAgreePrice { get; set; }
        public bool CanSeeMasters { get; set; }
        public bool CanSeeStatus { get; set; }
        public bool CanSeePriority { get; set; }
        public bool CanSeeAccepter { get; set; }
        public bool CanSeePrice { get; set; }
        public bool CanSeeSelfPrices { get; set; }
        public bool CanSeeReason { get; set; }
        public bool CanSeeHistory { get; set; }
        public bool CanEditIssuedRepairs { get; set; }
    }

    public class ApplicationPermissions
    {
        public bool HasUsersAccess { get; set; }
        public bool HasMastersAccess { get; set; }
        public bool HasReportsAccess { get; set; }
        public bool HasStatusesAccess { get; set; }
        public bool HasRolesAccess { get; set; }
        public bool HasClientsAccess { get; set; }
        public bool HasBreakagesAccess { get; set; }
    }
}
