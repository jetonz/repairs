﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RefactoringUpdate : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Repair", name: "BreakageId", newName: "Breakage_Id");
            RenameColumn(table: "dbo.Repair", name: "StatusId", newName: "Status_Id");
            RenameIndex(table: "dbo.Repair", name: "IX_StatusId", newName: "IX_Status_Id");
            RenameIndex(table: "dbo.Repair", name: "IX_BreakageId", newName: "IX_Breakage_Id");
            RenameColumn("dbo.Repair", "DateApply", "CreatedAt");
        }

        public override void Down()
        {
            RenameColumn("dbo.Repair", "CreatedAt", "DateApply");
            RenameIndex(table: "dbo.Repair", name: "IX_Breakage_Id", newName: "IX_BreakageId");
            RenameIndex(table: "dbo.Repair", name: "IX_Status_Id", newName: "IX_StatusId");
            RenameColumn(table: "dbo.Repair", name: "Status_Id", newName: "StatusId");
            RenameColumn(table: "dbo.Repair", name: "Breakage_Id", newName: "BreakageId");
        }
    }
}
