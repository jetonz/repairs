﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StatusName : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Statuses", "StatusName", "Name"); 
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Statuses", "Name", "StatusName");
        }
    }
}
