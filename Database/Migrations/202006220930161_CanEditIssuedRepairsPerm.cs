﻿namespace WorkShop.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CanEditIssuedRepairsPerm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Roles", "RepairPermissions_CanEditIssuedRepairs", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Roles", "RepairPermissions_CanEditIssuedRepairs");
        }
    }
}
