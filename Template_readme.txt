Для того щоб программа працювала, у коренні додатку має лежати файл з назвою template.xlsx 
Цей файл використовується як шаблон. У шаблоні доступні наступні ключеві слова які будуть замінені на реальну 
інформацію з ремонту:

REPAIR_ID                 |  Номер ремонту
REPAIR_FIO                |  ПІБ клієнта
REPAIR_PHONE              |  Номер телефону клієнта
REPAIR_MODEL              |  Модель телефона
REPAIR_IMEI               |  IMEI иелефона 
REPAIR_DEFFECT            |  Поломка
REPAIR_PHONEDESC          |  Примітки телефону (стан)
REPAIR_CREATED_AT         |  Час створення ремонту
REPAIR_ACCEPTED_BY        |  Хто прийняв ремонт