﻿using WorkShop.Models.Entities;
using System;
using System.Linq;
using WorkShop.Models;

namespace WorkShop.Views.Repairs
{
    public class RepairInfo
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string IMEI { get; set; }
        public string Deffect { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public string Master { get; set; }
        public string ClientName { get; set; }
        public string Accepter { get; set; }
        public DateTime DateApplied { get; set; }
        public DateTime? DateIssued { get; set; }
        public decimal RepairPrice { get; set; }  
        public decimal Expenses { get; set; }
        public bool IsRepairStatusIssued { get; set; }
        public ERepairUrgency RepairUrgency { get; set; }
        public DateTime UpdatedAt { get; set; }

        public void SetRepair(Repair repo)
        {
            Id = repo.Id;
            Model = repo.Model;
            IMEI = repo.Imei;
            Deffect = repo.Deffect;
            Reason = repo.Reason;
            IsRepairStatusIssued = repo.Status?.IsIssueStatus ?? false;
            RepairUrgency = repo.Urgency;
            UpdatedAt = repo.UpdatedAt;
            Status = repo.Status?.Name;
            Master = repo.Master?.Name;
            ClientName = repo.Client?.Name;
            Accepter = repo.Accepter?.Name;
            DateApplied = repo.CreatedAt;
            DateIssued = repo.DateIsued;
            RepairPrice = repo.Price;
            Expenses = repo.Price - repo.SuppliesPrice;
        }
    }


    public static class RepairInfoQueryExtention
    {

        public static IQueryable<RepairInfo> SelectRepairInfo(this IQueryable<Repair> repairs)
        {
            return repairs.Select(x => new RepairInfo
            {
                Id = x.Id,
                Model = x.Model,
                IMEI = x.Imei,
                Deffect = x.Deffect,
                Reason = x.Reason,
                IsRepairStatusIssued = (bool?)x.Status.IsIssueStatus ?? false,
                RepairUrgency = x.Urgency,
                UpdatedAt = x.UpdatedAt,
                Status = x.Status.Name,
                Accepter = x.Accepter.Name,
                Master = x.Master.Name,
                ClientName = x.Client.Name, 
                Expenses = x.WorkPrice + x.SuppliesPrice,
                DateApplied = x.CreatedAt,
                DateIssued = x.DateIsued,
                RepairPrice = x.Price
            });
        }
    }
}
