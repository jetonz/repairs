﻿namespace WorkShop.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RepairBreakageUpd : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Repair", name: "Breakage_Id", newName: "BreakageId");
            RenameIndex(table: "dbo.Repair", name: "IX_Breakage_Id", newName: "IX_BreakageId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Repair", name: "IX_BreakageId", newName: "IX_Breakage_Id");
            RenameColumn(table: "dbo.Repair", name: "BreakageId", newName: "Breakage_Id");
        }
    }
}
