﻿using WorkShop.Models.Entities;
using System.Data.Entity;

namespace WorkShop.Database
{
    public class RepairsContext : DbContext
    {
        public DbSet<Master> Masters { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Repair> Repairs { get; set; }
        public DbSet<RepairEditHistory> RepairEditHistories { get; set; }
        public DbSet<RepairMasterHistory> RepairMastersHistories { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<Breakage> Breakages { get; set; }

        public RepairsContext()
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Master>().ToTable("Masters");
            modelBuilder.Entity<Client>().ToTable("Clients");
            modelBuilder.Entity<Repair>().ToTable("Repair");
            modelBuilder.Entity<RepairEditHistory>().ToTable("RepairEditHistory");
            modelBuilder.Entity<RepairMasterHistory>().ToTable("RepairMastersHistory");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<Status>().ToTable("Statuses");
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Breakage>().ToTable("Breakages");

            modelBuilder.Entity<Repair>()
                .HasOptional(x => x.Accepter)
                .WithMany(x => x.AcceptedRepairs);

            modelBuilder.Entity<Repair>()
                .HasOptional(x => x.Breakage)
                .WithMany();

            modelBuilder.Entity<Repair>()
                .HasOptional(x => x.PriceAgreedBy)
                .WithMany();

            modelBuilder.Entity<Repair>()
                .HasRequired(x => x.CreatedBy)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Repair>()
                .Property(x => x.UpdatedAt)
                .HasColumnType("datetime2"); 


            modelBuilder.Entity<RepairEditHistory>()
                .Property(x => x.CreatedAt)
                .HasColumnType("datetime2");
        }
    }
}
