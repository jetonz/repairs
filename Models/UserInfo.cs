﻿using WorkShop.Models.Entities;
using System;

namespace WorkShop.Models
{
    public class UserInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Role CurrRole { get; set; }
        public string RoleName { get; set; }
       

        public UserInfo(User usr)
        {
            Name = usr.Name;
            Login = usr.Login;
            Password = usr.Password;
            RoleName = usr.Role.Name;
            CurrRole = usr.Role;
            Id = usr.Id;
        }

        public UserInfo()
        {
            CurrRole = new Role();
        }

    }
}
