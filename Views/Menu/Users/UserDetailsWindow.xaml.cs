﻿using WorkShop.Models;
using System.Windows;

namespace WorkShop.Views.Menu.Users
{
    /// <summary>
    /// Interaction logic for UserProfile.xaml
    /// </summary>
    public partial class UserDetailsWindow : Window
    {
        public UserDetailsWindow()
        {
            InitializeComponent();
            UserDetailsVeiwModel VM = new UserDetailsVeiwModel();
            this.DataContext = VM;

            this.ButtCancle.Click += (s, e) =>
            {
                this.Close();
            };

            VM.OnCloseView += () =>
            {
                this.Close();
            };

        }

        public UserDetailsWindow(UserInfo usr)
        {
            InitializeComponent();
            var VM = new UserDetailsVeiwModel(usr);

            this.DataContext = VM;

            this.ButtCancle.Click += (s, e) =>
            {
                this.Close();
            };

            VM.OnCloseView += () =>
            {
                this.Close();
            };

        }




    }
}
