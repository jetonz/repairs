﻿namespace WorkShop.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InvoiceNumber : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Repair", "Realization", "InvoiceNumber");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Repair", "InvoiceNumber", "Realization");
        }
    }
}
